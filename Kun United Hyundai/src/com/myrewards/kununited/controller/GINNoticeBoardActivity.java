package com.myrewards.kununited.controller;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.kununited.model.NoticeBoard;
import com.myrewards.kununited.service.BayfordServiceListener;
import com.myrewards.kununited.service.GrabItNowService;
import com.myrewards.kununited.utils.ApplicationConstants;
import com.myrewards.kununited.utils.Utility;

public class GINNoticeBoardActivity extends BaseActivity implements
		BayfordServiceListener {
	List<NoticeBoard> noticeBoardProductsList;
	NoticeBoardAdapter mAdapter;
	LayoutInflater inflater;
	public static int count, count1 = 0, count2 = 0, count3 = 0;
	View loading;
	ListView hotOffersListView;
	public static boolean noticecount = false;
	String catID = null;
	String location = null;
	String keyword = null;

	Boolean abc;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.results_list_gin);

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		setHeaderTitle(getResources().getString(R.string.my_notice_board));
		menuBtn = (Button) findViewById(R.id.menuBtnID);
		menuListView = (ListView) findViewById(R.id.menuListViewID);
		initialiseViews();
		loading = (View) findViewById(R.id.loading);
		hotOffersListView = (ListView) findViewById(R.id.resultsListViewID);
		noticeBoardProductsList = new ArrayList<NoticeBoard>();
		hotOffersListView.setOnItemClickListener(this);

		if (Utility
				.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			GrabItNowService.getGrabItNowService().sendNoticeBoardRequest(this);
		} else {
			loading.setVisibility(View.GONE);
			// The Custom Toast Layout Imported here
			LayoutInflater inflater = getLayoutInflater();
			View layout = inflater.inflate(R.layout.toast_no_netowrk,
					(ViewGroup) findViewById(R.id.custom_toast_layout_id));

			// The actual toast generated here.
			Toast toast = new Toast(getApplicationContext());
			toast.setDuration(Toast.LENGTH_LONG);
			toast.setView(layout);
			toast.show();
			// startActivity(new Intent(MyAccountActivity.this,
			// DashboardScreenActivity.class));
			GINNoticeBoardActivity.this.finish();
		}
	}

	public class NoticeBoardAdapter extends BaseAdapter {

		public NoticeBoardAdapter(GINNoticeBoardActivity noticeBoardActivity) {

		}

		@Override
		public int getCount() {
			return noticeBoardProductsList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(int pos, View view, ViewGroup arg2) {
			View resultsListRow = null;
			try {				
				if (resultsListRow == null) {
					inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					resultsListRow = (View) inflater.inflate(
							R.layout.results_list_item, null, false);
				}
				LinearLayout rowLL = (LinearLayout) resultsListRow.findViewById(R.id.resultListItemLLID);
				LinearLayout rowLL2 = (LinearLayout) resultsListRow.findViewById(R.id.resultListItemHariLLID);
				rowLL2.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL|Gravity.CENTER);
				rowLL2.getLayoutParams().height = (int) (Utility.screenHeight / 9.4);
				TextView productNameTV = (TextView) resultsListRow.findViewById(R.id.productTVID);
				productNameTV.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL|Gravity.CENTER);
				productNameTV.setTypeface(Utility.font_bold);
				TextView highlightTV = (TextView) resultsListRow.findViewById(R.id.offerTVID);
				highlightTV.setVisibility(View.GONE);
				productNameTV.setText(noticeBoardProductsList.get(pos).getSubject());
				if (pos % 2 == 0) {
					productNameTV.setTextColor(Color.parseColor("#FFFFFF"));
				} else if (pos % 2 == 1) {
					productNameTV.setTextColor(Color.parseColor("#427CDC"));
				}
				switch (pos % 3) {
				case 0:
					rowLL.setBackgroundResource(R.color.result_color_one);
					break;
				case 1:
					rowLL.setBackgroundResource(R.color.result_color_two);
					break;
				case 2:
					rowLL.setBackgroundResource(R.color.result_color_three);
					break;
				case 3:
					rowLL.setBackgroundResource(R.color.result_color_four);
					break;
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
					e.printStackTrace();
				}
				return null;
			}
			
			return resultsListRow;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View rowView, int pos,
			long arg3) {
		try {
			super.onItemClick(arg0, rowView, pos, arg3);
			noticecount = true;
			if (pos == 0) {

				if (count1 == 0)
					count1++;
			} else if (pos == 1) {
				if (count2 == 0)
					count2++;
			} else {
				if (count3 == 0)
					count3++;
			}

			if (menuListView.getVisibility() == ListView.GONE) {
				try {

					Intent detailsIntent = new Intent(GINNoticeBoardActivity.this,
							GINNoticeBoardDetailsActivity.class);
					detailsIntent.putExtra(ApplicationConstants.NOTICE_ID_KEY_GIN,
							noticeBoardProductsList.get(pos).getId());
					detailsIntent.putExtra(ApplicationConstants.COLOR_CODE_KEY,
							pos % 4);
					detailsIntent.putExtra(
							ApplicationConstants.NOTICE_NAME_KEY_GIN,
							noticeBoardProductsList.get(pos).getSubject());
					detailsIntent.putExtra(
							ApplicationConstants.NOTICE_DETAILS_KEY_GIN,
							noticeBoardProductsList.get(pos).getDetails());
					startActivity(detailsIntent);
				} catch (Exception e) {
					if (e != null) {
						Log.w("Hari-->DEBUG", e);
						e.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onServiceComplete(Object response, int eventType) {
		if (response != null) {
			if (response instanceof String) {
				// showErrorDialog(response.toString());
				Utility.showMessage(this, response.toString());
			} else {

				try {
					if (eventType == 12) {
						noticeBoardProductsList = (ArrayList<NoticeBoard>) response;

						mAdapter = new NoticeBoardAdapter(this);
						hotOffersListView.setAdapter(mAdapter);

					}
					loading.setVisibility(View.GONE);
				} catch (Exception e) {
					if (e != null) {
						Log.w("Hari-->DEBUG", e);
						e.printStackTrace();
					}
				}
			}
		}
	}

}
