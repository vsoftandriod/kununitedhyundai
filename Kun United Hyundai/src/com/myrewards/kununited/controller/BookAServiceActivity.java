package com.myrewards.kununited.controller;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.myrewards.kununited.controller.MyRoadsideAssist.MyAdapter;
import com.myrewards.kununited.utils.OpenHelper;
import com.myrewards.kununited.utils.Utility;

public class BookAServiceActivity extends Activity implements OnClickListener,
		OnItemSelectedListener, OnLongClickListener {
	Button send_btn, dateTimeBtn, scanBarBtn;
	Spinner serviceBtn;
	EditText regiField, vehicleField;
	OpenHelper openhelper;
	Cursor cr;
	SQLiteDatabase db;
	String dateTime;
	Button backButton, bookAServiceSendBtn;
	EditText commentsEdit;
	TextView titleTV, tv11;
	private final static int UPDATE_ACCOUNT_All_FIELDS = 11;

	Spinner locationsSP;
	/*String[] locationsArray = {
			"Select Location",
			"#1-8-870, Azamabad, Near VST,RTC X Roads, Hyderabad",
			"Plot No: 8, Mini Industrial Estate, Hafeezpet," + "\n"
					+ " Miyapur, Hyderabad.",
			"Plot No:285, Beside Lakshmi Gardens,ECIL X Road," + "\n"
					+ " RR Dist. - 500062",
			"B-4 Block 3, Beside Alkali Metal LTD, IDA, Uppal," + "\n"
					+ " Ranga Reddy - 500039.",
			"Door no 10-1, New gayatri nagar,Near SBH, Jillelaguda, Karmanghat,"
					+ "\n" + "Hyderabad 500079 ",
			"H.No:1-72/2/1/1, Plot No:2 & 13,Survey No:50, Gachi Bowli Main Road, "
					+ "\n" + "Serilingam Pally, Hyderabad - 500032.",
			 };*/
	String[] locationsArray = {
			"Select Location",
			"RTC X Roads",
			"Miyapur",
			"ECIL X Road",
			"Uppal",
			"Karmanghat",
			"Gachi Bowli Main Road",
			 };
	RadioButton RB;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.book_a_service);

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		titleTV = (TextView) findViewById(R.id.titleTVID);

		titleTV.setTypeface(Utility.font_bold);

		titleTV.setText(getResources().getString(R.string.book_a_service));

		scanBarBtn = (Button) findViewById(R.id.scanBtnID);
		scanBarBtn.setVisibility(View.GONE);

		openhelper = new OpenHelper(this);
		db = openhelper.getReadableDatabase();

		cr = db.query("mycar", null, null, null, null, null, null);
		cr.moveToFirst();
		cr.moveToPosition(0);

		regiField = (EditText) findViewById(R.id.bookAServiceRegTVID);

		regiField.setTypeface(Utility.font_reg);

		regiField.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		vehicleField = (EditText) findViewById(R.id.bookAServiceVehicleTVID);

		vehicleField.setTypeface(Utility.font_reg);

		vehicleField.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		if (cr != null) {
			try {
				if (cr.getString(cr.getColumnIndex("carMake")) != null
						&& cr.getString(cr.getColumnIndex("carModel")) != null
						&& cr.getString(cr.getColumnIndex("carRegistration")) != null) {
					vehicleField
							.setText(cr.getString(cr
									.getColumnIndex("carModel")));
					regiField.setText(cr.getString(cr.getColumnIndex("carMake"))
									+ "/"
									+ cr.getString(cr
											.getColumnIndex("carRegistration"))
							);
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
					e.printStackTrace();
				}
			}
		}
		if (cr.getCount() == 0) {
			regiField.setText("");
		}
		dateTimeBtn = (Button) findViewById(R.id.bookAServiceDateTimeID);
		dateTimeBtn.setTypeface(Utility.font_reg);
		dateTimeBtn.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		serviceBtn = (Spinner) findViewById(R.id.bookaServiceSpinnerID);
		serviceBtn.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		backButton = (Button) findViewById(R.id.backBtnID);
		backButton.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backButton.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		commentsEdit = (EditText) findViewById(R.id.commentsEditID);
		commentsEdit.setTypeface(Utility.font_reg);

		commentsEdit.setOnLongClickListener(this);
		commentsEdit.getLayoutParams().height = (int) (Utility.screenHeight / 6.0);
		bookAServiceSendBtn = (Button) findViewById(R.id.bookAServiceSendBtnID);
		bookAServiceSendBtn.getLayoutParams().width = (int) (Utility.screenWidth / 4.8);
		bookAServiceSendBtn.getLayoutParams().height = (int) (Utility.screenHeight / 18.7);
		bookAServiceSendBtn.setOnClickListener(this);
		backButton.setOnClickListener(this);
		dateTimeBtn.setOnClickListener(this);

		ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(
				this, R.array.BASserviceItems,
				android.R.layout.simple_spinner_item);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		ArrayAdapter<CharSequence> adapter2 =new ArrayAdapter<CharSequence>(this,android.R.layout.simple_spinner_item,locationsArray);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		serviceBtn.setAdapter(adapter1);
		
		locationsSP = (Spinner) findViewById(R.id.locationsSpinnerID);
		locationsSP.getLayoutParams().height = (int) (Utility.screenHeight / 16);
	//	locationsSP.setAdapter(new MyAdapter(this, R.layout.custom_spinner,locationsArray));
		locationsSP.setAdapter(adapter2);
		
	//	locationBtn.setAdapter(adapter2);
	//	locationBtn.setOnItemSelectedListener(this);
	}
	public class MyAdapter extends ArrayAdapter<String> {

		String[] tempItems;

		public MyAdapter(Context ctx, int txtViewResourceId, String[] objects) {
			super(ctx, txtViewResourceId, objects);

			tempItems = objects;
		}

		@Override
		public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
			return getCustomView(position, cnvtView, prnt);
		}

		@Override
		public View getView(final int pos, View cnvtView, ViewGroup prnt) {
			return getCustomView(pos, cnvtView, prnt);
		}

		public View getCustomView(int position, View convertView,
				ViewGroup parent) {
			LayoutInflater inflater = getLayoutInflater();
			View mySpinner = inflater.inflate(R.layout.custom_spinner_for_bas, parent,
					false);
			TextView main_text = (TextView) mySpinner
					.findViewById(R.id.categoryTVID);

			// main_text.setText("djsdfjdfbvkjnfdgbnfdgkndbnkb"+"\n"+"jdbjnfgbjnfgfgjnhgf"+"dfsjvbndfkjnbbndfkj");

			main_text.setText(tempItems[position]);
			 RB = (RadioButton) mySpinner
					.findViewById(R.id.categoryRBID);
			if(position==locationsSP.getSelectedItemPosition()) 
			{
				RB.setChecked(true);
			}

			return mySpinner;
		}
	}
	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.bookAServiceDateTimeID) {
			showDialog(1);
		} else if (v.getId() == R.id.bookaServiceSpinnerID) {
		}  else if (v.getId() == R.id.backBtnID) {
			finish();
		} else if (v.getId() == R.id.bookAServiceSendBtnID) {
			try {
				String dateTime = dateTimeBtn.getText().toString();
				String serviceSP = serviceBtn.getSelectedItem().toString();
				String locationSP = locationsSP.getSelectedItem().toString();
				String comments = commentsEdit.getText().toString();

			
				if (dateTime.length() < 1 || locationSP.length()<1
						|| serviceSP.equals("Select Service")
						|| comments.length() < 1) {
					showDialog(UPDATE_ACCOUNT_All_FIELDS);
				} else {
					
					String toClientemail = getResources().getString(
							R.string.kun_bookaservice_email);
					Utility.BASemailText = "Vehicle :: "
							+regiField.getText().toString()  + "\n"
							+ "Registration :: "
							+vehicleField.getText().toString()
							+ regiField.getText().toString() + "\n"
							+ "Date and Time :: "
							+ dateTimeBtn.getText().toString() + "\n"
							+ "Type of Service :: "
							+ serviceBtn.getSelectedItem().toString()
							+ "\n" + "Location :: " + locationSP
							+ "\n" + "Comments :: "
							+ commentsEdit.getText().toString();
					Intent email = new Intent(Intent.ACTION_SEND);
					email.putExtra(Intent.EXTRA_EMAIL,
							new String[] { toClientemail });
					email.putExtra(Intent.EXTRA_SUBJECT, "Book A Service");
					email.putExtra(Intent.EXTRA_TEXT, Utility.BASemailText);

					// need this to prompts email client only
					email.setType("message/rfc822");
					startActivity(Intent.createChooser(email,
							"Choose an Email client :"));
					
					/*
					if (bayfordLocEmail.equals("Camberwell VW")) {
						String toClientemail = getResources().getString(
								R.string.bayford_vw_camberwell_email);
						Utility.BASemailText = "Registration :: "
								+ regiField.getText().toString() + "\n"
								+ "Vehicle :: "
								+ vehicleField.getText().toString() + "\n"
								+ "Date and Time :: "
								+ dateTimeBtn.getText().toString() + "\n"
								+ "Type of Service :: "
								+ serviceBtn.getSelectedItem().toString()
								+ "\n" + "Location :: " + bayfordLocEmail
								+ "\n" + "Comments :: "
								+ commentsEdit.getText().toString();
						Intent email = new Intent(Intent.ACTION_SEND);
						email.putExtra(Intent.EXTRA_EMAIL,
								new String[] { toClientemail });
						email.putExtra(Intent.EXTRA_SUBJECT, "Book A Service");
						email.putExtra(Intent.EXTRA_TEXT, Utility.BASemailText);

						// need this to prompts email client only
						email.setType("message/rfc822");
						startActivity(Intent.createChooser(email,
								"Choose an Email client :"));
					} else if (bayfordLocEmail.equals("City Peugeot")) {
						String toClientemail = getResources().getString(
								R.string.bayford_city_peugeot_email);
						Utility.BASemailText = "Registration :: "
								+ regiField.getText().toString() + "\n"
								+ "Vehicle :: "
								+ vehicleField.getText().toString() + "\n"
								+ "Date and Time :: "
								+ dateTimeBtn.getText().toString() + "\n"
								+ "Type of Service :: "
								+ serviceBtn.getSelectedItem().toString()
								+ "\n" + "Location :: " + bayfordLocEmail
								+ "\n" + "Comments :: "
								+ commentsEdit.getText().toString();
						Intent email = new Intent(Intent.ACTION_SEND);
						email.putExtra(Intent.EXTRA_EMAIL,
								new String[] { toClientemail });
						email.putExtra(Intent.EXTRA_SUBJECT, "Book A Service");
						email.putExtra(Intent.EXTRA_TEXT, Utility.BASemailText);

						// need this to prompts email client only
						email.setType("message/rfc822");
						startActivity(Intent.createChooser(email,
								"Choose an Email client :"));
					} else if (bayfordLocEmail.equals("Coburg Ford")) {
						String toClientemail1 = getResources().getString(
								R.string.bayford_coburg_ford_email1);
						String toClientemail2 = getResources().getString(
								R.string.bayford_coburg_ford_email2);
						Utility.BASemailText = "Registration :: "
								+ regiField.getText().toString() + "\n"
								+ "Vehicle :: "
								+ vehicleField.getText().toString() + "\n"
								+ "Date and Time :: "
								+ dateTimeBtn.getText().toString() + "\n"
								+ "Type of Service :: "
								+ serviceBtn.getSelectedItem().toString()
								+ "\n" + "Location :: " + bayfordLocEmail
								+ "\n" + "Comments :: "
								+ commentsEdit.getText().toString();
						Intent email = new Intent(Intent.ACTION_SEND);
						email.putExtra(Intent.EXTRA_EMAIL, new String[] {
								toClientemail1, toClientemail2 });
						email.putExtra(Intent.EXTRA_SUBJECT, "Book A Service");
						email.putExtra(Intent.EXTRA_TEXT, Utility.BASemailText);

						// need this to prompts email client only
						email.setType("message/rfc822");
						startActivity(Intent.createChooser(email,
								"Choose an Email client :"));
					} else if (bayfordLocEmail.equals("Coburg KIA")) {
						// String
						// toClientemail1=getResources().getString(R.string.bayford_coburg_ford_email1);
						String toClientemail2 = getResources().getString(
								R.string.bayford_coburg_ford_email2);
						Utility.BASemailText = "Registration :: "
								+ regiField.getText().toString() + "\n"
								+ "Vehicle :: "
								+ vehicleField.getText().toString() + "\n"
								+ "Date and Time :: "
								+ dateTimeBtn.getText().toString() + "\n"
								+ "Type of Service :: "
								+ serviceBtn.getSelectedItem().toString()
								+ "\n" + "Location :: " + bayfordLocEmail
								+ "\n" + "Comments :: "
								+ commentsEdit.getText().toString();
						Intent email = new Intent(Intent.ACTION_SEND);
						email.putExtra(Intent.EXTRA_EMAIL,
								new String[] { toClientemail2 });
						email.putExtra(Intent.EXTRA_SUBJECT, "Book A Service");
						email.putExtra(Intent.EXTRA_TEXT, Utility.BASemailText);

						// need this to prompts email client only
						email.setType("message/rfc822");
						startActivity(Intent.createChooser(email,
								"Choose an Email client :"));
					} else if (bayfordLocEmail.equals("Epping VW")) {
						String toClientemail1 = getResources().getString(
								R.string.bayford_epping_vw_email1);
						String toClientemail2 = getResources().getString(
								R.string.bayford_epping_vw_email2);
						Utility.BASemailText = "Registration :: "
								+ regiField.getText().toString() + "\n"
								+ "Vehicle :: "
								+ vehicleField.getText().toString() + "\n"
								+ "Date and Time :: "
								+ dateTimeBtn.getText().toString() + "\n"
								+ "Type of Service :: "
								+ serviceBtn.getSelectedItem().toString()
								+ "\n" + "Location :: " + bayfordLocEmail
								+ "\n" + "Comments :: "
								+ commentsEdit.getText().toString();
						Intent email = new Intent(Intent.ACTION_SEND);
						email.putExtra(Intent.EXTRA_EMAIL, new String[] {
								toClientemail1, toClientemail2 });
						email.putExtra(Intent.EXTRA_SUBJECT, "Book A Service");
						email.putExtra(Intent.EXTRA_TEXT, Utility.BASemailText);

						// need this to prompts email client only
						email.setType("message/rfc822");
						startActivity(Intent.createChooser(email,
								"Choose an Email client :"));
					} else if (bayfordLocEmail.equals("Epping Ford")) {
						String toClientemail1 = getResources().getString(
								R.string.bayford_epping_ford_email);
						// String
						// toClientemail2=getResources().getString(R.string.bayford_epping_vw_email2);
						Utility.BASemailText = "Registration :: "
								+ regiField.getText().toString() + "\n"
								+ "Vehicle :: "
								+ vehicleField.getText().toString() + "\n"
								+ "Date and Time :: "
								+ dateTimeBtn.getText().toString() + "\n"
								+ "Type of Service :: "
								+ serviceBtn.getSelectedItem().toString()
								+ "\n" + "Location :: " + bayfordLocEmail
								+ "\n" + "Comments :: "
								+ commentsEdit.getText().toString();
						Intent email = new Intent(Intent.ACTION_SEND);
						email.putExtra(Intent.EXTRA_EMAIL,
								new String[] { toClientemail1 });
						email.putExtra(Intent.EXTRA_SUBJECT, "Book A Service");
						email.putExtra(Intent.EXTRA_TEXT, Utility.BASemailText);

						// need this to prompts email client only
						email.setType("message/rfc822");
						startActivity(Intent.createChooser(email,
								"Choose an Email client :"));
					} else if (bayfordLocEmail.equals("South Yarra Peugeot")) {
						String toClientemail1 = getResources().getString(
								R.string.bayford_south_yerra_email);
						Utility.BASemailText = "Registration :: "
								+ regiField.getText().toString() + "\n"
								+ "Vehicle :: "
								+ vehicleField.getText().toString() + "\n"
								+ "Date and Time :: "
								+ dateTimeBtn.getText().toString() + "\n"
								+ "Type of Service :: "
								+ serviceBtn.getSelectedItem().toString()
								+ "\n" + "Location :: " + bayfordLocEmail
								+ "\n" + "Comments :: "
								+ commentsEdit.getText().toString();
						Intent email = new Intent(Intent.ACTION_SEND);
						email.putExtra(Intent.EXTRA_EMAIL,
								new String[] { toClientemail1 });
						email.putExtra(Intent.EXTRA_SUBJECT, "Book A Service");
						email.putExtra(Intent.EXTRA_TEXT, Utility.BASemailText);

						// need this to prompts email client only
						email.setType("message/rfc822");
						startActivity(Intent.createChooser(email,
								"Choose an Email client :"));
					}
				*/}
			} catch (Exception e) {
				if (e != null) {
					Log.w("HARI-->DEBUG", e);
				}
			}
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			if (id == 11) {
				AlertDialog fieldsDialog = null;
				switch (id) {
				case UPDATE_ACCOUNT_All_FIELDS:
					LayoutInflater mli = LayoutInflater.from(this);
					View allFieldsView = mli.inflate(
							R.layout.dailog_layout_update_all_man_fields, null);
					AlertDialog.Builder allFalert = new AlertDialog.Builder(
							this);
					allFalert.setCancelable(false);
					allFalert.setView(allFieldsView);
					fieldsDialog = allFalert.create();
					break;
				}
				return fieldsDialog;
			}

		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
		return super.onCreateDialog(id);
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		super.onPrepareDialog(id, dialog);
		try {
			if (id == 11) {
				switch (id) {
				case UPDATE_ACCOUNT_All_FIELDS:
					final AlertDialog alertDialog2 = (AlertDialog) dialog;
					TextView alertTitle = (TextView) alertDialog2
							.findViewById(R.id.alertTitlesTVID);

					alertTitle.setTypeface(Utility.font_bold);

					tv11 = (TextView) alertDialog2
							.findViewById(R.id.manFieldAccountTVID);
					tv11.setTypeface(Utility.font_reg);

					Button okbutton = (Button) alertDialog2
							.findViewById(R.id.manFieldAccountOKBtnID);
					okbutton.setTypeface(Utility.font_bold);
					alertDialog2.setCancelable(false);
					okbutton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							alertDialog2.dismiss();
						}
					});
					break;
				}
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
	}

	@Override
	protected Dialog onCreateDialog(int id, Bundle args) {
		try {
			Calendar c = Calendar.getInstance();
			// int seconds = c.get(Calendar.SECOND);
			int minutes = c.get(Calendar.MINUTE);
			int hours = c.get(Calendar.HOUR);
			int years = c.get(Calendar.YEAR);
			int months = c.get(Calendar.MONTH);
			int days = c.get(Calendar.DAY_OF_MONTH);
			// int AM_orPM = c.get(Calendar.AM_PM);
			if (id == 1) {
				return new DatePickerDialog(this, new DatePick(), years, months,
						days);
			} else if (id == 2) {
				return new TimePickerDialog(this, new TimeSet1(), hours, minutes,
						false);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
		return super.onCreateDialog(id, args);
	}

	class DatePick implements DatePickerDialog.OnDateSetListener {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			dateTime = dayOfMonth + "/" + monthOfYear + "/" + year;
			showDialog(2);
		}
	}

	class TimeSet1 implements OnTimeSetListener {
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			// Calendar cal = Calendar.getInstance();
			dateTimeBtn.setText(dateTime + "\t" + hourOfDay + ":" + minute);
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int pos,
			long arg3) {
		// if (pos==0) {

		// String camberwellEmail=locationBtn.getSelectedItem().toString();
		// }

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

	@Override
	public boolean onLongClick(View v) {
		boolean returnValue = false;
		try {
			EditText ed = (EditText) v;
			int stringLength = ed.getText().length();
			returnValue = Utility.copyPasteMethod2(v, stringLength);
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->DEBUG", e);
			}
			returnValue = false;
		}
		return returnValue;
	}

}
