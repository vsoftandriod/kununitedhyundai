package com.myrewards.kununited.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.kununited.utils.Utility;

@SuppressWarnings("deprecation")
public class LikeActivity extends Activity implements OnClickListener {
	Button mailBtn, callBtn, backBtn, scanBarBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.like_me_screen);

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		TextView titleTV = (TextView) findViewById(R.id.titleTVID);
		titleTV.setText(getResources().getString(R.string.like_title));
titleTV.setTypeface(Utility.font_bold);
		scanBarBtn = (Button) findViewById(R.id.scanBtnID);
		scanBarBtn.setVisibility(View.GONE);

		backBtn = (Button) findViewById(R.id.backBtnID);
		backBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);

		mailBtn = (Button) findViewById(R.id.contactUsMailBtnID);
		mailBtn.getLayoutParams().width = (int) (Utility.screenWidth / 2.2); // 218
		mailBtn.getLayoutParams().height = (int) (Utility.screenHeight / 3.66);

		callBtn = (Button) findViewById(R.id.contactUsCallBtnID);
		callBtn.getLayoutParams().width = (int) (Utility.screenWidth / 2.2);
		callBtn.getLayoutParams().height = (int) (Utility.screenHeight / 3.66);

		backBtn.setOnClickListener(this);
		mailBtn.setOnClickListener(this);
		callBtn.setOnClickListener(this);
		;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.backBtnID:
			LikeActivity.this.finish();
			break;
		case R.id.contactUsMailBtnID:
			if (Utility
					.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse("https://www.facebook.com/KunUnitedHyundai"));
				startActivity(i);

			} else {

				showToast();
			}
			break;
		case R.id.contactUsCallBtnID:
			if (Utility
					.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse("https://twitter.com/KunUnitedHyd"));
				startActivity(i);
			} else {
				showToast();
			}
			break;

		default:
			break;
		}
	}

	private void showToast() {
		// The Custom Toast Layout Imported here
		LayoutInflater inflater = getLayoutInflater();
		View layout = inflater.inflate(R.layout.toast_no_netowrk,
				(ViewGroup) findViewById(R.id.custom_toast_layout_id));
		// layout.getBackground().setAlpha(128); // 50% transparent

		// The actual toast generated here.
		Toast toast = new Toast(getApplicationContext());
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setView(layout);
		toast.show();
	}
}
