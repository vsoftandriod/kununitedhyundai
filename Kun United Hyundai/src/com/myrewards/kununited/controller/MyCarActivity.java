package com.myrewards.kununited.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.myrewards.kununited.utils.OpenHelper;
import com.myrewards.kununited.utils.Utility;

public class MyCarActivity extends Activity implements OnLongClickListener {
	OpenHelper openhelper;
	EditText edt1, edt2, edt3, edt4, edt5;
	Button save, scanBarBtn;
	Cursor cr;
	SQLiteDatabase db;
	TextView titleTV, tv11;
	Button backButton;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 1, 0, "car details");
		return true;

	}

	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == 1) {
			Intent in = new Intent(getApplicationContext(), MyCarDetailsActivity.class);
			startActivity(in);
		}
		return true;
	}

	protected void onCreate(Bundle savedInstanceState) {

		// SQLiteDatabase database;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mycar2);

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		titleTV = (TextView) findViewById(R.id.titleTVID);
		titleTV.setTypeface(Utility.font_bold);
		titleTV.setText(getResources().getString(R.string.my_car));

		scanBarBtn = (Button) findViewById(R.id.scanBtnID);
		scanBarBtn.setVisibility(View.GONE);

		backButton = (Button) findViewById(R.id.backBtnID);
		backButton.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backButton.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		TextView tv1 = (TextView) findViewById(R.id.carMake);
		tv1.setTypeface(Utility.font_reg);

		TextView tv2 = (TextView) findViewById(R.id.carModel);
		tv2.setTypeface(Utility.font_reg);

		TextView tv3 = (TextView) findViewById(R.id.carReg);
		tv3.setTypeface(Utility.font_reg);

		TextView tv4 = (TextView) findViewById(R.id.carColor);
		tv4.setTypeface(Utility.font_reg);

		TextView tv5 = (TextView) findViewById(R.id.carYear);
		tv5.setTypeface(Utility.font_reg);

		edt1 = (EditText) findViewById(R.id.carMakeText);
		edt1.setTypeface(Utility.font_bold);
		edt1.setOnLongClickListener(this);
		edt1.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		edt2 = (EditText) findViewById(R.id.carModelText);
		edt2.setTypeface(Utility.font_bold);
		edt2.setOnLongClickListener(this);
		edt2.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		edt3 = (EditText) findViewById(R.id.carRegText);
		edt3.setTypeface(Utility.font_bold);
		edt3.setOnLongClickListener(this);
		edt3.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		edt4 = (EditText) findViewById(R.id.carColorText);
		edt4.setTypeface(Utility.font_bold);
		edt4.setOnLongClickListener(this);
		edt4.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		edt5 = (EditText) findViewById(R.id.carYearText);
		edt5.setTypeface(Utility.font_bold);
		edt5.setOnLongClickListener(this);
		edt5.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		save = (Button) findViewById(R.id.saveBtn);
		save.getLayoutParams().width = (int) (Utility.screenWidth / 4.0);
		save.getLayoutParams().height = (int) (Utility.screenHeight / 17.0);
		save.setOnClickListener(new myListener());
		openhelper = new OpenHelper(this);
		db = openhelper.getReadableDatabase();

		cr = db.query("mycar", null, null, null, null, null, null);
		cr.moveToFirst();
		cr.moveToPosition(0);
		try {
			if (cr.getString(cr.getColumnIndex("carMake")) != null
					&& cr.getString(cr.getColumnIndex("carModel")) != null
					&& cr.getString(cr.getColumnIndex("carRegistration")) != null
					&& cr.getString(cr.getColumnIndex("carColor")) != null
					&& cr.getString(cr.getColumnIndex("carYear")) != null) {
				edt1.setText(cr.getString(cr.getColumnIndex("carMake")));
				edt2.setText(cr.getString(cr.getColumnIndex("carModel")));
				edt3.setText(cr.getString(cr.getColumnIndex("carRegistration")));
				edt4.setText(cr.getString(cr.getColumnIndex("carColor")));
				edt5.setText(cr.getString(cr.getColumnIndex("carYear")));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	class myListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			try {
				{
					

					String carMake1 = null;
					String carModel2 = null;
					String carRegisteration3 = null;
					String carColor4 = null;
					String carYear5 = null;

					carMake1 = edt1.getEditableText().toString();
					carModel2 = edt2.getEditableText().toString();
					carRegisteration3 = edt3.getEditableText().toString();
					carColor4 = edt4.getEditableText().toString();
					carYear5 = edt5.getEditableText().toString();

					if (carMake1.trim().length() != 0
							&& carModel2.trim().length() != 0
							&& carRegisteration3.trim().length() != 0
							&& carColor4.trim().length() != 0
							&& carYear5.trim().length() != 0) {
						
						db = openhelper.getWritableDatabase();
						db.execSQL("DELETE FROM " + "mycar");
						SQLiteStatement insert;
						String insertstmt = "INSERT INTO mycar (carMake,carModel,carRegistration,carColor,carYear) values (?,?,?,?,?)";
						insert = db.compileStatement(insertstmt);
						insert.bindString(1, edt1.getEditableText().toString());
						insert.bindString(2, edt2.getEditableText().toString());
						insert.bindString(3, edt3.getEditableText().toString());
						insert.bindString(4, edt4.getEditableText().toString());
						insert.bindString(5, edt5.getEditableText().toString());
						insert.execute();
						
						
						showDialog(1);
					} else {
						showDialog(2);
					}
					db.close();
				}

			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			if (id == 1) {
				AlertDialog fieldsDialog = null;
				LayoutInflater mli = LayoutInflater.from(this);
				View allFieldsView = mli.inflate(
						R.layout.dailog_layout_update_all_man_fields, null);
				AlertDialog.Builder allFalert = new AlertDialog.Builder(this);
				allFalert.setCancelable(false);
				allFalert.setView(allFieldsView);
				fieldsDialog = allFalert.create();
				return fieldsDialog;
			} else if (id == 2) {
				AlertDialog fieldsDialog = null;
				LayoutInflater mli = LayoutInflater.from(this);
				View allFieldsView = mli.inflate(
						R.layout.dailog_layout_update_all_man_fields, null);
				AlertDialog.Builder allFalert = new AlertDialog.Builder(this);
				allFalert.setCancelable(false);
				allFalert.setView(allFieldsView);
				fieldsDialog = allFalert.create();
				return fieldsDialog;
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
			return null;
		}
		return super.onCreateDialog(id);
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		try {
			super.onPrepareDialog(id, dialog);
			if (id == 1) {
				final AlertDialog alertDialog2 = (AlertDialog) dialog;

				tv11 = (TextView) alertDialog2
						.findViewById(R.id.manFieldAccountTVID);
				tv11.setTypeface(Utility.font_reg);
				tv11.setText("Data Saved Successfully");
				TextView alertTitlesTV = (TextView) alertDialog2
						.findViewById(R.id.alertTitlesTVID);
				alertTitlesTV.setTypeface(Utility.font_bold);

				alertTitlesTV.setText("Success !");
				Button okbutton = (Button) alertDialog2
						.findViewById(R.id.manFieldAccountOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				alertDialog2.setCancelable(false);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog2.dismiss();
					}
				});
			} else if (id == 2) {
				final AlertDialog alertDialog2 = (AlertDialog) dialog;
				TextView alertTitle = (TextView) alertDialog2
						.findViewById(R.id.alertTitlesTVID);
				alertTitle.setTypeface(Utility.font_bold);

				tv11 = (TextView) alertDialog2
						.findViewById(R.id.manFieldAccountTVID);
				// tv11.setText("All fields are mandetory");
				Button okbutton = (Button) alertDialog2
						.findViewById(R.id.manFieldAccountOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				alertDialog2.setCancelable(false);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog2.dismiss();
					}
				});
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean onLongClick(View v) {
		boolean returnValue = false;
		try {
			EditText ed = (EditText) v;
			int stringLength = ed.getText().length();
			returnValue = Utility.copyPasteMethod2(v, stringLength);
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
		return returnValue;
	}

}
