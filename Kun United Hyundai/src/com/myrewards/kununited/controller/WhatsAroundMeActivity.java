package com.myrewards.kununited.controller;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.NYXDigital.NiceSupportMapFragment;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.myrewards.kununited.model.Product;
import com.myrewards.kununited.service.BayfordServiceListener;
import com.myrewards.kununited.service.GrabItNowService;
import com.myrewards.kununited.utils.ApplicationConstants;
import com.myrewards.kununited.utils.Utility;

/**
 * @author HARI This class contains the information about the near by products
 *         and Map view with all overlay information using Network provider.
 * 
 */

@SuppressLint("ShowToast")
@SuppressWarnings("unused")
public class WhatsAroundMeActivity extends FragmentActivity implements
		InfoWindowAdapter, OnClickListener, BayfordServiceListener,
		LocationListener, OnItemClickListener, OnInfoWindowClickListener,
		OnMarkerClickListener {
	private View loading;
	private boolean isClientLogo = false;
	private GoogleMap googleMap;
	private ArrayList<Product> product;
	private Marker markerforCL, marker1, marker2, marker3, marker4, marker5,
			marker6, marker7;
	private LatLng location1, location2, location3, location4, location5,
			location6, location7;
	private Location location;
	private Context mContext;

	private int i;
	private int productsLength = 0;
	TextView titleTV;

	List<String> tempList;
	List<String> tempList2;
	String temp, temp2;
	private boolean tempStatus = false;

	GPSTracker mGPS = null;

	//Initialize to a non-valid zoom value
	private float previousZoomLevel = -1.0f;
	private boolean isZooming = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.whats_around_me);
		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);
		titleTV = (TextView) findViewById(R.id.titleTVID);
		titleTV.setTypeface(Utility.font_bold);
		titleTV.setText("Nearby Rewards");
		loading = (View) findViewById(R.id.loading);
		tempList = new ArrayList<String>();
		product = new ArrayList<Product>();

		try {
			initilizeMaps();
		} catch (Exception e) {
			Log.w("Hari--->", e);
		}
	}

	private void initilizeMaps() {
		if (googleMap == null) {
			NiceSupportMapFragment mapFragment = (NiceSupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.mapfragmentId);
			googleMap = mapFragment.getMap();
			mapFragment.setPreventParentScrolling(false);
		}

		// check if map is created successfully or not
		if (googleMap == null) {
			Toast.makeText(getApplicationContext(),
					"Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
		} else {
			googleMap.setMyLocationEnabled(true);
			googleMap.getUiSettings().setMyLocationButtonEnabled(true);
			if (mGPS == null) {
				mGPS = new GPSTracker(WhatsAroundMeActivity.this);
			}

			// check if mGPS object is created or not
			if (mGPS != null && location == null) {
				location = mGPS.getLocation();
			}

			// check if location is created or not
			if (location != null) {
				onLocationChanged(location);
				googleMap.getUiSettings().setMyLocationButtonEnabled(true);

				if (Utility
						.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					GrabItNowService.getGrabItNowService().sendNearestLatLonRequest(WhatsAroundMeActivity.this, mGPS.getLatitude(), mGPS.getLongitude());
				//	GrabItNowService.getGrabItNowService().sendNearestLatLonRequest(WhatsAroundMeActivity.this, 17.40593,78.49907);
				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater
							.inflate(
									R.layout.toast_no_netowrk,
									(ViewGroup) findViewById(R.id.custom_toast_layout_id));

					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_LONG);
					toast.setView(layout);
					toast.show();
					WhatsAroundMeActivity.this.finish();
				}
			} else {
				loading.setVisibility(View.GONE);
				showDialog(1207);
				// mGPS.showSettingsAlert();
			}
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
	}

	@Override
	public void onLocationChanged(Location location) {
		try {
			LatLng currentlocation = new LatLng(location.getLatitude(),
					location.getLongitude());
			markerforCL = googleMap.addMarker(new MarkerOptions()
					.position(currentlocation).title("I am Here:")
					.icon(BitmapDescriptorFactory.fromResource(R.drawable.me)));
			googleMap.setOnMarkerClickListener(this);
			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(currentlocation, 12);
			googleMap.moveCamera(cameraUpdate);
			googleMap.animateCamera(cameraUpdate);
			
	
			googleMap.setOnCameraChangeListener(getCameraChangeListener());
		} catch (Exception e) {
			Log.w("Hari-->", e);
		}
	}
	public OnCameraChangeListener getCameraChangeListener()
	{
	    return new OnCameraChangeListener() 
	    {
	        @Override
	        public void onCameraChange(CameraPosition position) 
	        {
	            Log.d("Zoom", "Zoom: " + position.zoom);

	            if(previousZoomLevel != position.zoom)
	            {
	                isZooming = true;
	            }

	            previousZoomLevel = position.zoom;
	        }
	    };
	}
	@Override
	public void onProviderDisabled(String provider) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	@SuppressWarnings("unchecked")
	@Override
	public void onServiceComplete(Object response, int eventType) {
		try {
			if (response != null) {
				if (response instanceof String) {
					Utility.showMessage(this, response.toString());
				} else {
					if (eventType == 8) {
						try {
							product = (ArrayList<Product>) response;

							if (product != null && product.size() > 0) {
								showAllPins();
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		} catch (Exception e) {
			Log.w("Hari-->", e);
		}
	}

	private void showAllPins() {
		try {
			Double nlat = 0.0, nlong = 0.0;
			final Double COORDINATE_OFFSET = 0.00002;
			productsLength = product.size();
			//Toast.makeText(getApplicationContext(), "Pins = "+productsLength, 300000).show();
			if (productsLength > 400) {
				productsLength = (productsLength / 2);
			}
			DecimalFormat dtime = new DecimalFormat("#.#####");
			for (i = 0; i < productsLength; i++) {
				// this logic is for arranging pins having sam lat,lang values
				if (i == 0)
					Toast.makeText(this, "Loading please wait.....", 5000).show();
				if (i == 0) {
					nlat = Double.valueOf(dtime.format(Double.parseDouble(product
							.get(i).getLatitude())));

					nlong = Double.valueOf(dtime.format(Double.parseDouble(product
							.get(i).getLongitude())));
				} else {
					nlat = Double.valueOf(dtime.format(Double.parseDouble(product
							.get(i).getLatitude())));

					nlong = Double.valueOf(dtime.format(Double.parseDouble(product
							.get(i).getLongitude())));

					for (int k = 0; k < tempList.size(); k++) {
						if (tempList.get(k).equals(nlat + "," + nlong)) {

							if (!(i > 30)) {
								nlat = nlat + COORDINATE_OFFSET
										+ (((0.001) * (i / 25.0)) / 10.0);
								nlong = nlong + COORDINATE_OFFSET
										+ (((0.001) * (i / 25.0)) / 10.0);
							} else {
								nlat = nlat + COORDINATE_OFFSET
										+ (((0.001) * ((i % 29) / 25.0)) / 10.0);
								nlong = nlong + COORDINATE_OFFSET
										+ (((0.001) * ((i % 29) / 25.0)) / 10.0);
							}
							tempStatus = true;
							break;
						}
					}
					if (tempStatus == false) {
						nlat = Double.valueOf(dtime.format(Double.parseDouble(product.get(i).getLatitude())));
						nlong = Double.valueOf(dtime.format(Double.parseDouble(product.get(i).getLongitude())));
					}
				}

				if (Integer.parseInt(product.get(i).getPinType()) == 1) {
					marker1 = googleMap.addMarker(new MarkerOptions()
							.position(new LatLng(nlat, nlong))
							.icon(BitmapDescriptorFactory
									.fromResource(R.drawable.pin_type1))
							.title(product.get(i).getName()));
					marker1.showInfoWindow();
					googleMap.setOnMarkerClickListener(this);
					googleMap.setOnInfoWindowClickListener(this);

				} else if (Integer.parseInt(product.get(i).getPinType()) == 2) {
					marker2 = googleMap.addMarker(new MarkerOptions()
							.position(new LatLng(nlat, nlong))
							.icon(BitmapDescriptorFactory
									.fromResource(R.drawable.pin_type2))
							.title(product.get(i).getName()));
					marker2.showInfoWindow();
					googleMap.setOnMarkerClickListener(this);
					googleMap.setOnInfoWindowClickListener(this);
				} else if (Integer.parseInt(product.get(i).getPinType()) == 3) {

					marker3 = googleMap.addMarker(new MarkerOptions()
							.position(new LatLng(nlat, nlong))
							.icon(BitmapDescriptorFactory
									.fromResource(R.drawable.pin_type3))
							.title(product.get(i).getName()));

					marker3.showInfoWindow();
					googleMap.setOnMarkerClickListener(this);
					googleMap.setOnInfoWindowClickListener(this);

				} else if (Integer.parseInt(product.get(i).getPinType()) == 4) {
					marker4 = googleMap.addMarker(new MarkerOptions()
							.icon(BitmapDescriptorFactory
									.fromResource(R.drawable.pin_type4))
							.position(new LatLng(nlat, nlong))
							.title(product.get(i).getName()));
					marker4.showInfoWindow();
					googleMap.setOnMarkerClickListener(this);
					googleMap.setOnInfoWindowClickListener(this);
				} else if (Integer.parseInt(product.get(i).getPinType()) == 5) {
					marker5 = googleMap.addMarker(new MarkerOptions()
							.position(new LatLng(nlat, nlong))
							.icon(BitmapDescriptorFactory
									.fromResource(R.drawable.pin_type6))
							.title(product.get(i).getName()));
					marker5.showInfoWindow();
					googleMap.setOnMarkerClickListener(this);
					googleMap.setOnInfoWindowClickListener(this);

				} else if (Integer.parseInt(product.get(i).getPinType()) == 6) {
					marker6 = googleMap.addMarker(new MarkerOptions()
							.icon(BitmapDescriptorFactory
									.fromResource(R.drawable.pin_type7))
							.position(new LatLng(nlat, nlong))
							.title(product.get(i).getName()));
					marker6.showInfoWindow();
					googleMap.setOnMarkerClickListener(this);
					googleMap.setOnInfoWindowClickListener(this);
				} else if (Integer.parseInt(product.get(i).getPinType()) == 7) {
					marker7 = googleMap.addMarker(new MarkerOptions()
							.icon(BitmapDescriptorFactory
									.fromResource(R.drawable.pin_type5))
							.position(new LatLng(nlat, nlong))
							.title(product.get(i).getName()));
					marker7.showInfoWindow();
					googleMap.setOnMarkerClickListener(this);
					googleMap.setOnInfoWindowClickListener(this);
				}
				product.get(i).setLatitude(Double.toString(nlat));
				product.get(i).setLongitude(Double.toString(nlong));
				tempList.add(nlat + "," + nlong);
				tempStatus = false;
				Log.v("pins", nlat + "," + nlong);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->", e);
			}
			loading.setVisibility(View.GONE);
		}
		loading.setVisibility(View.GONE);
	}

	@Override
	public void onClick(View v) {
	}

	@Override
	public boolean onMarkerClick(Marker marker) {
		return false;
	}

	@Override
	public void onInfoWindowClick(Marker marker) {
		try {
			for (int j = 0; j < productsLength; j++) {
				Log.v("hai", "this is 1");
				DecimalFormat dtime = new DecimalFormat("#.#####");
				marker.setTitle(product.get(j).getName());

				Double val = marker.getPosition().latitude;
				val = Double.valueOf(dtime.format(val));
				System.out.println((Double.valueOf(dtime.format(Double
								.parseDouble((product.get(j).getLatitude())))))
								+ " "
								+ Double.valueOf(dtime.format(marker.getPosition().latitude)));
				if ((Double.valueOf(dtime.format(Double.parseDouble((product.get(j)
						.getLatitude()))))).equals(Double.valueOf(dtime
						.format(marker.getPosition().latitude)))) {
					Log.v("hai", "this is entered 1");

					System.out.println(marker.getPosition().latitude + "      "
							+ marker.getPosition().latitude);
					Intent detailsIntent = new Intent(WhatsAroundMeActivity.this,
							ProductDetailsActivity.class);
					detailsIntent.putExtra(ApplicationConstants.PRODUCT_ID_KEY,
							product.get(j).getId());
					detailsIntent.putExtra(ApplicationConstants.PRODUCT_NAME_KEY,
							product.get(j).getName());
					startActivity(detailsIntent);
					break;
				}
			}
		} catch (Exception e) {
			Log.w("Hari-->", e);
		}
	}

	@Override
	public View getInfoContents(Marker marker) {
		return null;
	}

	@Override
	public View getInfoWindow(Marker marker) {
		return null;
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			if (id == 1207) {
				AlertDialog GPSAlert = null;
				LayoutInflater liDelete = LayoutInflater.from(WhatsAroundMeActivity.this);
				View deleteFavView = liDelete.inflate(
						R.layout.dialog_layout_delete_favorite, null);
				AlertDialog.Builder adbDeleteFav = new AlertDialog.Builder(WhatsAroundMeActivity.this);
				adbDeleteFav.setView(deleteFavView);
				GPSAlert = adbDeleteFav.create();
				return GPSAlert;
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
		return super.onCreateDialog(id);
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		try {
			if (id == 1207) {
				final AlertDialog alt3 = (AlertDialog) dialog;
				TextView alertTilteTV = (TextView) alt3
						.findViewById(R.id.firstLoginCEPUTitleTVID);
				alertTilteTV.setTypeface(Utility.font_bold);
				alertTilteTV.setText("GPS Settings !");
				TextView tv22 = (TextView) alt3.findViewById(R.id.deleteFavTVID);
				tv22.setTypeface(Utility.font_reg);
				tv22.setText("GPS is not enabled. Do you want to go to settings menu?");
				Button deleteFavYesBtn = (Button) alt3
						.findViewById(R.id.delete_fav_yesBtnID);
				deleteFavYesBtn.setTypeface(Utility.font_bold);
				deleteFavYesBtn.setText("Settings");
				Button deleteNoFavBtn = (Button) alt3
						.findViewById(R.id.delete_fav_noBtnID);
				deleteNoFavBtn.setTypeface(Utility.font_bold);
				deleteNoFavBtn.setText("Cancel");
				deleteFavYesBtn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						try {
							Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
							WhatsAroundMeActivity.this.startActivity(intent);
							WhatsAroundMeActivity.this.finish();
						} catch (Exception e) {
							Log.w("Hari-->", e);
						}
						alt3.dismiss();
					}
				});
				deleteNoFavBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						alt3.dismiss();
					}
				});
			}
			super.onPrepareDialog(id, dialog);
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
	}
}
