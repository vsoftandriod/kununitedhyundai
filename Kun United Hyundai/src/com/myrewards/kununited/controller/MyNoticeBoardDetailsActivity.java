package com.myrewards.kununited.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.myrewards.kununited.model.ProductNotice;
import com.myrewards.kununited.utils.ApplicationConstants;
import com.myrewards.kununited.utils.Utility;

public class MyNoticeBoardDetailsActivity extends Activity implements
		OnClickListener {
	public static final String DETAILS = "details";
	ProductNotice productNotice;
	Button backBtn, scanBarBtn;
	TextView titleTV;
	TextView subjectTV;
	WebView detailsWV;
	String noticeId;
	String noticeSubject = null;
	String noticeDetails = null;

	@SuppressWarnings("unused")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_noticeboard_details);
		View mynoticeHeader = (View) findViewById(R.id.headerRLID);
		mynoticeHeader.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);
		mynoticeHeader.setVisibility(View.VISIBLE);

		titleTV = (TextView) findViewById(R.id.titleTVID);
		titleTV.setTypeface(Utility.font_bold);
		titleTV.setText(getResources().getString(R.string.my_notice_board));

		scanBarBtn = (Button) findViewById(R.id.scanBtnID);
		scanBarBtn.setVisibility(View.GONE);

		backBtn = (Button) findViewById(R.id.backBtnID);
		backBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		backBtn.setOnClickListener(this);

		if (getIntent() != null) {
			try {
				int colorResource = 0;
				Bundle bundle = getIntent().getExtras();
				if (bundle != null) {
					Bundle b = getIntent().getExtras();
					productNotice = (ProductNotice) b.getSerializable(DETAILS);
					noticeId = bundle
							.getString(ApplicationConstants.NOTICE_ID_KEY);
					noticeSubject = bundle
							.getString(ApplicationConstants.NOTICE_NAME_KEY);
					noticeDetails = bundle
							.getString(ApplicationConstants.NOTICE_DETAILS_KEY);
					colorResource = bundle
							.getInt(ApplicationConstants.COLOR_CODE_KEY);

					subjectTV = (TextView) findViewById(R.id.subjectTVID);
					subjectTV.setTypeface(Utility.font_bold);
					subjectTV.setText(productNotice.getSubject());
					subjectTV.setOnClickListener(this);
					/*	TextView detailsTV = (TextView) findViewById(R.id.detailsTVID);
					detailsTV.setTypeface(Utility.font_reg);
					detailsTV.setText(Html.fromHtml(productNotice.getDetails()));*/
					
					loadInWebView(productNotice.getDetails());
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
					e.printStackTrace();
				}
			}
		}
	}

	@SuppressWarnings("unused")
	private void setHeaderFooterDimentions() {
		try {
			ImageView headerIV = (ImageView) findViewById(R.id.headerIVID);
			// ImageView footerIV=(ImageView)findViewById(R.id.footerIVID);
			headerIV.getLayoutParams().height = Utility.screenHeight / 26;
			// footerIV.getLayoutParams().height=Utility.screenHeight/24;
			RelativeLayout headerRLID = (RelativeLayout) findViewById(R.id.headerRLID);
			headerRLID.getLayoutParams().height = Utility.screenHeight / 13;
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
	}

	/**
	 * get data sent from previous screen/Activity
	 */
	@SuppressWarnings("unused")
	private void getIntentData() {
		try {
			if (getIntent().getExtras() != null) {
				Bundle b = getIntent().getExtras();
				productNotice = (ProductNotice) b.getSerializable(DETAILS);
				populateUI();
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
	}

	private void populateUI() {
		try {
			subjectTV = (TextView) findViewById(R.id.subjectTVID);
			subjectTV.setTypeface(Utility.font_bold);
			subjectTV.setText(productNotice.getSubject());
			subjectTV.setOnClickListener(this);
			TextView detailsTV = (TextView) findViewById(R.id.detailsTVID);
			detailsTV.setTypeface(Utility.font_reg);
			detailsTV.setText(Html.fromHtml(productNotice.getDetails()));
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.backBtnID:
			try {
				startActivity(new Intent(MyNoticeBoardDetailsActivity.this,
						MyNoticeBoardActivity.class));
				MyNoticeBoardDetailsActivity.this.finish();
			} catch (Exception e) {
				startActivity(new Intent(this, DashboardScreenActivity.class));
				MyNoticeBoardDetailsActivity.this.finish();
			}
			break;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			try {
				startActivity(new Intent(MyNoticeBoardDetailsActivity.this,
						MyNoticeBoardActivity.class));
				MyNoticeBoardDetailsActivity.this.finish();
			} catch (Exception e) {
				startActivity(new Intent(this, DashboardScreenActivity.class));
				MyNoticeBoardDetailsActivity.this.finish();
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	private void loadInWebView(String webContent) {
		WebView webView = (WebView) findViewById(R.id.detailsTVID);
	
		webView.getSettings()
		.setJavaScriptEnabled(true);
webView.getSettings().setAllowFileAccess(true);
webView.getSettings()
		.setLoadsImagesAutomatically(true);
webView.getSettings().setUserAgentString(Locale.getDefault().getLanguage());
WebSettings settings = webView.getSettings();
settings.setDefaultTextEncodingName("utf-8");
webView.setWebViewClient(new WebViewClient());
webView.setWebViewClient(new WebViewClient() {

	@Override
	public boolean shouldOverrideUrlLoading(
			WebView view, String url) {
		if (url.startsWith("tel:")) {
			Intent intent = new Intent(
					Intent.ACTION_DIAL, Uri.parse(url));
			startActivity(intent);
		} else if (url.startsWith("http:")
				|| url.startsWith("https:")) {
			Intent intent = new Intent(
					Intent.ACTION_VIEW, Uri.parse(url));
			startActivity(intent);
		} else if (url.startsWith("mailto:")) {
			MailTo mt = MailTo.parse(url);
			Intent i = EmailIntent(MyNoticeBoardDetailsActivity.this,mt.getTo(), mt.getSubject(),
					mt.getBody(), mt.getCc());
			startActivity(i);
			view.reload();
			return true;
		} else {
			view.loadUrl(url);
		}
		return true;
	}
});
/*
 * String summary = Html.fromHtml(
 * product.getDetails() + "\n" + "\n" +
 * product.getText()).toString();
 */
String summary = "<html><body style=\"font-family:Helvetica;line-height:20px\">"
		+ webContent
		+ "</body></html>";

summary = summary.replaceAll("//", "");
// create text file
if (!Environment.getExternalStorageState()
		.equals(Environment.MEDIA_MOUNTED))
	Log.d("KUN", "No SDCARD");
else {
	File direct = new File(
			Environment
					.getExternalStorageDirectory()
					+ "/KUN");

	if (!direct.exists()) {
		if (direct.mkdir()) {
			// directory is created;
		}
	}

	try {
		File root = new File(
				Environment
						.getExternalStorageDirectory()
						+ "/KUN");
		if (root.canWrite()) {
			File file = new File(root,
					"KUNnoticedetails.html");
			FileWriter fileWriter = new FileWriter(
					file);
			BufferedWriter out = new BufferedWriter(
					fileWriter);
			if (summary.contains("<iframe")) {
				try {
					int a = summary
							.indexOf("<iframe");
					int b = summary
							.indexOf("</iframe>");
					summary = summary
							.replace(
									summary.subSequence(
											a,
											b),
									"");
				} catch (Exception e) {
					if (e != null) {
						e.printStackTrace();
						
					}
				}
			}
			out.write(summary);
			out.close();
		}
	} catch (IOException e) {
		if (e != null) {
			e.printStackTrace();
		
		}
	}
	
	if (!Environment.getExternalStorageState()
			.equals(Environment.MEDIA_MOUNTED)) {
		Log.d("KUN", "No SDCARD");
	} else {

		webView.loadUrl("file://"
				+ Environment
						.getExternalStorageDirectory()
				+ "/KUN"
				+ "/KUNnoticedetails.html");
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onReceivedError(
					WebView view, int errorCode,
					String description,
					String failingUrl) {
				Log.i("WEB_VIEW_TEST",
						"error code:" + errorCode);

				super.onReceivedError(view,
						errorCode, description,
						failingUrl);
			}
		});

		
	}
}
}
	
	
	
	public static Intent EmailIntent(Context context, String address,
			String subject, String body, String cc) 
	{
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.putExtra(Intent.EXTRA_EMAIL, new String[] { address });
		intent.putExtra(Intent.EXTRA_TEXT, body);
		intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		intent.putExtra(Intent.EXTRA_CC, cc);
		intent.setType("message/rfc822");
		return intent;
	}
}
