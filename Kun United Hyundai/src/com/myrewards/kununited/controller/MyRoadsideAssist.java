package com.myrewards.kununited.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.NYXDigital.NiceSupportMapFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.myrewards.kununited.model.DirectionsJSONParser;
import com.myrewards.kununited.utils.Utility;

public class MyRoadsideAssist extends FragmentActivity implements
		LocationListener, android.location.LocationListener, OnClickListener,
		OnMarkerClickListener, OnInfoWindowClickListener {
	Button backButton, scanBarBtn;
	// EditText locationDescText;
	GoogleMap googleMap;
	Marker startPerc;
	String str1;
	TextView numberTV, titleTV;
	final private static int CALL_NUMBER = 1;
	Location location;

	GPSTracker mGPS = null;

	// Initialize to a non-valid zoom value
	private float previousZoomLevel = -1.0f;
	private boolean isZooming = false;

	Spinner locationsSP;
	String[] locationsArray = {
			"Select Location",
			"#1-8-870, Azamabad, Near VST,RTC X Roads, Hyderabad",
			"Plot No: 8, Mini Industrial Estate, Hafeezpet," + "\n"
					+ " Miyapur, Hyderabad.",
			"Plot No:285, Beside Lakshmi Gardens,ECIL X Road," + "\n"
					+ " RR Dist. - 500062",
			"B-4 Block 3, Beside Alkali Metal LTD, IDA, Uppal," + "\n"
					+ " Ranga Reddy - 500039.",
			"Door no 10-1, New gayatri nagar,Near SBH, Jillelaguda, Karmanghat,"
					+ "\n" + "Hyderabad 500079 ",
			"H.No:1-72/2/1/1, Plot No:2 & 13,Survey No:50, Gachi Bowli Main Road, "
					+ "\n" + "Serilingam Pally, Hyderabad - 500032.",
			"All Locations" };
	String[] locationsPositions = { " ", "17.411448/78.498291",
			"17.482484/78.363067", "17.466145/78.562796",
			"17.398404/78.553309", "17.340959/78.535664",
			"17.385044/78.486671", " " };

	String[] branchLocations = { "", "RTC X Roads", "Miyapur", "ECIL X Road",
			"Uppal", "Karmanghat", "Serilingam Pally", "" };

	// String[]
	// callDataArray={" ","040-27668841","040-40320500","040-40315511","040-40315511","040-24092324","040-23000620"," "};
	private String[] callDataArray = { " ", "09246-537791", "09246-537792",
			"09246-537793", "09246-537795", "09246-537796", "09246-537790", " " };

	RadioButton RB;
	TextView tvDistanceDuration;

	LatLng currentLatLng;
	LatLng currentDestLatLng;

	int colors[] = { Color.RED, Color.GREEN, Color.BLUE, Color.BLACK,
			Color.MAGENTA, Color.CYAN };

	private static int currentColorValue = 0;

	private boolean allLocationsLoop = false;

	List<Double> distancesList;

	// Marker[] markersArray;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_roadside_assist);

		/*
		 * StrictMode.ThreadPolicy policy = new
		 * StrictMode.ThreadPolicy.Builder().permitAll().build();
		 * StrictMode.setThreadPolicy(policy);
		 */

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		titleTV = (TextView) findViewById(R.id.titleTVID);
		titleTV.setTypeface(Utility.font_bold);
		titleTV.setText(getResources().getString(R.string.my_road_side_assists));

		scanBarBtn = (Button) findViewById(R.id.scanBtnID);
		scanBarBtn.setVisibility(View.GONE);

		LinearLayout mapheader = (LinearLayout) findViewById(R.id.mapParentLLID);
		mapheader.getLayoutParams().height = (int) (Utility.screenHeight / 10.5);

		backButton = (Button) findViewById(R.id.backBtnID);
		backButton.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backButton.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		numberTV = (TextView) findViewById(R.id.roadContactNumberTVID);

		locationsSP = (Spinner) findViewById(R.id.locationsSpinnerID);
		// locationsSP.getLayoutParams().height = (int) (Utility.screenHeight /
		// 15);
		locationsSP.setAdapter(new MyAdapter(this, R.layout.custom_spinner,
				locationsArray));

		tvDistanceDuration = (TextView) findViewById(R.id.distanceTVID);

		backButton.setOnClickListener(this);
		numberTV.setOnClickListener(this);

		// Getting Google Play availability status
		int status = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(getBaseContext());

		// Showing status
		if (status != ConnectionResult.SUCCESS) { // Google Play Services are
													// not available

			int requestCode = 10;
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this,
					requestCode);
			dialog.show();

		} else { // Google Play Services are available
			try {
				initilizeMaps();
			} catch (Exception e) {
				Log.w("Hari--->", e);
			}
		}

		// spinner code here

		locationsSP.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				if (position == 0 || position == 7) {
					googleMap.clear();
					if (position == 0) {
if(location!=null)
						onLocationChanged(location);
					} else {

						allLocationsLoop = true;
						try {
							if(location!=null)
							showAllLocations();
						} catch (Exception e) {

						}

					}
				} else {
					// showLocation(locationsPositions[position], position);

					if (location != null) {

						googleMap.clear();

						onLocationChanged(location);

						LatLng source = new LatLng(location.getLatitude(),
								location.getLongitude());

						// LatLng source=new LatLng(22.657275,76.794434);

						LatLng dest = new LatLng(Double
								.parseDouble(locationsPositions[position]
										.split("/")[0]), Double
								.parseDouble(locationsPositions[position]
										.split("/")[1]));
						// LatLng dest=new LatLng(25.765595, 77.036133);
						currentDestLatLng = dest;
						try {
							drawPathBetweenPoints(source, dest);
						} catch (Exception e) {

						}
					}

				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		googleMap.setOnInfoWindowClickListener(this);
	}

	private void showLocation(String loc, int pos) {
		Double lat = Double.parseDouble(loc.split("/")[0]), lon = Double
				.parseDouble(loc.split("/")[1]);

		googleMap.clear();
		onLocationChanged(location);

		LatLng ex = new LatLng(lat, lon);
		Marker marker = googleMap.addMarker(new MarkerOptions()
				.position(ex)
				.title("Call: " + callDataArray[pos])
				.snippet("")

				.icon(BitmapDescriptorFactory
						.fromResource(R.drawable.shambhi_map_pin)));

		marker.showInfoWindow();
		googleMap.setOnMarkerClickListener(this);
		googleMap.setOnInfoWindowClickListener(this);

		CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(ex, 12);

		// Showing the current location in Google Map
		googleMap.moveCamera(cameraUpdate);

		// Zoom in the Google Map
		googleMap.animateCamera(cameraUpdate);

	}

	private void showAllLocations() {

		googleMap.clear();
		onLocationChanged(location);

		// markersArray=new Marker[6];

		distancesList = new ArrayList<Double>();

		for (int i = 1; i < 7; i++) {
			Double lat = Double.parseDouble(locationsPositions[i].split("/")[0]), 
				   lon = Double.parseDouble(locationsPositions[i].split("/")[1]);

			LatLng ex = new LatLng(lat, lon);
			/*
			 * Marker marker = googleMap.addMarker(new MarkerOptions()
			 * .position(ex) .title("Call: " + callDataArray[i]) .snippet("")
			 * 
			 * .icon(BitmapDescriptorFactory
			 * .fromResource(R.drawable.shambhi_map_pin)));
			 */

			LatLng source = new LatLng(location.getLatitude(),
					location.getLongitude());

			drawPathBetweenPoints(source, ex);

			// marker.showInfoWindow();

			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		CameraUpdate cameraUpdate = CameraUpdateFactory
				.newLatLngZoom(
						new LatLng(location.getLatitude(), location
								.getLongitude()), 11);

		// Showing the current location in Google Map
		googleMap.moveCamera(cameraUpdate);

		// Zoom in the Google Map
		googleMap.animateCamera(cameraUpdate);
	}

	public class MyAdapter extends ArrayAdapter<String> {

		String[] tempItems;

		public MyAdapter(Context ctx, int txtViewResourceId, String[] objects) {
			super(ctx, txtViewResourceId, objects);

			tempItems = objects;
		}

		@Override
		public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
			return getCustomView(position, cnvtView, prnt);
		}

		@Override
		public View getView(final int pos, View cnvtView, ViewGroup prnt) {
			return getCustomView(pos, cnvtView, prnt);
		}

		public View getCustomView(int position, View convertView,
				ViewGroup parent) {
			LayoutInflater inflater = getLayoutInflater();
			View mySpinner = inflater.inflate(R.layout.custom_spinner, parent,
					false);
			RelativeLayout spinnerRl = (RelativeLayout) mySpinner
					.findViewById(R.id.spinnerRLID);
			spinnerRl.getLayoutParams().height = (int) (Utility.screenHeight / 12);
			TextView main_text = (TextView) mySpinner
					.findViewById(R.id.categoryTVID);

			// main_text.setText("djsdfjdfbvkjnfdgbnfdgkndbnkb"+"\n"+"jdbjnfgbjnfgfgjnhgf"+"dfsjvbndfkjnbbndfkj");

			main_text.setText(tempItems[position]);
			RB = (RadioButton) mySpinner.findViewById(R.id.categoryRBID);
			// RB.getLayoutParams().height = (int) (Utility.screenWidth / 10);
			// RB.getLayoutParams().width = (int) (Utility.screenWidth / 10);
			if (position == locationsSP.getSelectedItemPosition()) {
				RB.setChecked(true);
			}

			return mySpinner;
		}
	}

	private void initilizeMaps() {
		if (googleMap == null) {
			NiceSupportMapFragment mapFragment = (NiceSupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.mapfragmentId);
			googleMap = mapFragment.getMap();
			mapFragment.setPreventParentScrolling(false);
		}

		// check if map is created successfully or not
		if (googleMap == null) {
			Toast.makeText(getApplicationContext(),
					"Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
		} else {
			googleMap.setMyLocationEnabled(true);

			if (mGPS == null) {
				mGPS = new GPSTracker(MyRoadsideAssist.this);
			}

			// check if mGPS object is created or not
			if (mGPS != null && location == null) {
				location = mGPS.getLocation();
			}

			// check if location is created or not
			if (location != null) {
				googleMap.setMyLocationEnabled(true);
				onLocationChanged(location);
			} else {
				showDialog(1207);
			}
		}
	}

	@SuppressWarnings("unused")
	@Override
	public void onLocationChanged(Location arg0) {
		try {
			if (arg0 != null) {
				try {
					currentLatLng = new LatLng(arg0.getLatitude(),
							arg0.getLongitude());
					startPerc = googleMap.addMarker(new MarkerOptions()
							.position(currentLatLng)
							.title("I am Here")
							.snippet("")

							.icon(BitmapDescriptorFactory
									.fromResource(R.drawable.me)));

					LatLng latLng = new LatLng(arg0.getLatitude(),
							arg0.getLongitude());
					CameraPosition cameraPosition = new CameraPosition.Builder()
							.target(latLng) // Sets the center of the map to
											// Mountain View
							.zoom(11) // Sets the zoom
							.bearing(90) // Sets the orientation of the camera
											// to east
							.tilt(30) // Sets the tilt of the camera to 30
										// degrees
							.build(); // Creates a CameraPosition from the
										// builder

					CameraUpdate cameraUpdate = CameraUpdateFactory
							.newLatLngZoom(latLng, 11);

					// Showing the current location in Google Map
					googleMap.moveCamera(cameraUpdate);

					// Zoom in the Google Map
					googleMap.animateCamera(cameraUpdate);
					googleMap
							.setOnCameraChangeListener(getCameraChangeListener());
					Geocoder geo = new Geocoder(
							MyRoadsideAssist.this.getApplicationContext(),
							Locale.getDefault());
					try {
						List<Address> ads = geo.getFromLocation(
								arg0.getLatitude(), arg0.getLongitude(), 1);

						String s1 = ads.get(0).getSubLocality(), s2 = ads
								.get(0).getLocality(), s3 = ads.get(0)
								.getSubAdminArea(), s4 = ads.get(0)
								.getAdminArea(), s5 = ads.get(0)
								.getCountryName();
						String str[] = { s1, s2, s3, s4, s5 };

						str1 = "";
						int count = 0;
						for (int i = 0; i < str.length; i++) {
							if (str[i] != null && str[i] != ""
									&& str[i] != "\0") {
								if (str1 == "") {
									str1 = str[i];
								} else {
									str1 = str1 + "," + str[i];
								}
								count++;
							}
							if (count == 2) {
								break;
							}
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (Exception e) {
					Log.w("Hari--->", e);
				}
			}
		} catch (Exception e) {
			Log.w("Hari--->", e);
		}
	}

	private void drawPathBetweenPoints(LatLng origin, LatLng dest) {
		/*
		 * MarkerOptions options=new MarkerOptions(); options.position(dest);
		 * 
		 * options.icon(BitmapDescriptorFactory.defaultMarker(
		 * BitmapDescriptorFactory.HUE_GREEN)); googleMap.addMarker(options);
		 */
		// Getting URL to the Google Directions API
		String url = getDirectionsUrl(origin, dest);
		// "https://maps.googleapis.com/maps/api/directions/json?origin=17.4694376,78.5091475&destination=25.765595,77.036133&sensor=false";
		// getDirectionsUrl(origin, dest);

		DownloadTask downloadTask = new DownloadTask();

		// Start downloading json data from Google Directions API
		downloadTask.execute(url);
	}

	private String getDirectionsUrl(LatLng origin, LatLng dest) {

		// Origin of route
		String str_origin = "origin=" + origin.latitude + ","
				+ origin.longitude;

		// Destination of route
		String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

		// Sensor enabled
		String sensor = "sensor=false";

		// Building the parameters to the web service
		String parameters = str_origin + "&" + str_dest + "&" + sensor;

		// Output format
		String output = "json";

		// Building the url to the web service
		String url = "https://maps.googleapis.com/maps/api/directions/"
				+ output + "?" + parameters;

		return url;
	}

	/** A method to download json data from url */
	private String downloadUrl(String strUrl) throws IOException {
		String data = "";
		InputStream iStream = null;
		HttpURLConnection urlConnection = null;
		try {
			URL url = new URL(strUrl);

			// Creating an http connection to communicate with url
			urlConnection = (HttpURLConnection) url.openConnection();

			// Connecting to url
			urlConnection.connect();

			// Reading data from url
			iStream = urlConnection.getInputStream();

			BufferedReader br = new BufferedReader(new InputStreamReader(
					iStream));

			StringBuffer sb = new StringBuffer();

			String line = "";
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

			data = sb.toString();

			br.close();

		} catch (Exception e) {
			Log.d("Exception while downloading url", e.toString());
		} finally {
			iStream.close();
			urlConnection.disconnect();
		}
		return data;
	}

	// Fetches data from url passed
	private class DownloadTask extends AsyncTask<String, Void, String> {

		// Downloading data in non-ui thread
		@Override
		protected String doInBackground(String... url) {

			// For storing data from web service
			String data = "";

			try {
				// Fetching the data from web service
				data = downloadUrl(url[0]);
			} catch (Exception e) {
				Log.d("Background Task", e.toString());
			}
			return data;
		}

		// Executes in UI thread, after the execution of
		// doInBackground()
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			ParserTask parserTask = new ParserTask();

			// Invokes the thread for parsing the JSON data
			parserTask.execute(result);

		}
	}

	/** A class to parse the Google Places in JSON format */
	private class ParserTask extends
			AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

		// Parsing the data in non-ui thread
		@Override
		protected List<List<HashMap<String, String>>> doInBackground(
				String... jsonData) {

			JSONObject jObject;
			List<List<HashMap<String, String>>> routes = null;

			try {
				jObject = new JSONObject(jsonData[0]);
				DirectionsJSONParser parser = new DirectionsJSONParser();

				// Starts parsing data
				routes = parser.parse(jObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return routes;
		}

		// Executes in UI thread, after the parsing process
		@Override
		protected void onPostExecute(List<List<HashMap<String, String>>> result) {
			ArrayList<LatLng> points = null;
			PolylineOptions lineOptions = null;
			MarkerOptions markerOptions = new MarkerOptions();
			String distance = "";
			String duration = "";

			if (result.size() < 1) {
				Toast.makeText(getBaseContext(), "No Points",
						Toast.LENGTH_SHORT).show();
				// drawDisplacement();
				return;
			}

			// Traversing through all the routes
			for (int i = 0; i < result.size(); i++) {
				points = new ArrayList<LatLng>();
				lineOptions = new PolylineOptions();

				// Fetching i-th route
				List<HashMap<String, String>> path = result.get(i);

				// Fetching all the points in i-th route
				for (int j = 0; j < path.size(); j++) {
					HashMap<String, String> point = path.get(j);

					if (j == 0) { // Get distance from the list
						distance = (String) point.get("distance");
						continue;
					} else if (j == 1) { // Get duration from the list
						duration = (String) point.get("duration");
						continue;
					}

					double lat = Double.parseDouble(point.get("lat"));
					double lng = Double.parseDouble(point.get("lng"));
					LatLng position = new LatLng(lat, lng);

					points.add(position);
				}

				// Adding marker here

				for (int j = 1; j < 7; j++) {
					Double lat = Double.parseDouble(locationsPositions[j]
							.split("/")[0]), lon = Double
							.parseDouble(locationsPositions[j].split("/")[1]);

					lat = Double
							.valueOf(new DecimalFormat("#.###").format(lat));
					lon = Double
							.valueOf(new DecimalFormat("#.###").format(lon));

					LatLng temp = points.get(points.size() - 1);
					double tempLat = Double.valueOf(new DecimalFormat("#.###")
							.format(temp.latitude));
					double tempLon = Double.valueOf(new DecimalFormat("#.###")
							.format(temp.longitude));

					if ((lat < (tempLat + 0.005) && lat > (tempLat - 0.005))
							&& (lon < (tempLon + 0.005) && lon > (tempLon - 0.005))) {

						String title = "Call: " + callDataArray[j] + "\n" + ","
								+ branchLocations[j];

						LatLng ex = new LatLng(lat, lon);
						Marker marker = googleMap
								.addMarker(new MarkerOptions()
										.position(ex)
										.title(title)
										.snippet("Distance:" + distance)

										.icon(BitmapDescriptorFactory
												.fromResource(R.drawable.shambhi_map_pin)));

						marker.showInfoWindow();

						break;

					}

				}

				// Adding all the points in the route to LineOptions
				lineOptions.addAll(points);
				lineOptions.width(10);
				if (currentColorValue < 6) {
					lineOptions.color(colors[currentColorValue]);
					currentColorValue++;
				} else {

					currentColorValue = 0;
					lineOptions.color(colors[currentColorValue]);

				}

			}

			char[] distaceChArray = new char[distance.length()];
			int j = 0;
			for (int i = 0; i < distance.length(); i++) {
				char c = distance.charAt(i);
				if (!(Character.isLetter(c) || (c == ' '))) {
					distaceChArray[j] = c;
					j++;
				}
			}
			StringBuilder sb = new StringBuilder();
			sb.append(distaceChArray);
			Double actualDistance = Double.parseDouble(sb.toString());
			/*
			 * CameraUpdate cameraUpdate = null;
			 * if(Integer.parseInt(distance)<25) { cameraUpdate =
			 * CameraUpdateFactory.newLatLngZoom(currentLatLng, 12); } else
			 * if(Integer.parseInt(distance)<50) { cameraUpdate =
			 * CameraUpdateFactory.newLatLngZoom(currentLatLng, 10); } else
			 * if(Integer.parseInt(distance)<75) { cameraUpdate =
			 * CameraUpdateFactory.newLatLngZoom(currentLatLng, 8); } else {
			 * cameraUpdate = CameraUpdateFactory.newLatLngZoom(currentLatLng,
			 * 6); }
			 * 
			 * // Showing the current location in Google Map
			 * googleMap.moveCamera(cameraUpdate);
			 * 
			 * // Zoom in the Google Map googleMap.animateCamera(cameraUpdate);
			 */
			// distance=new DecimalFormat("#.##").format(distance);

			tvDistanceDuration.setText("Distance:" + distance + ", Duration:"
					+ duration);

			if (allLocationsLoop) {
				distancesList.add(actualDistance);

				if (distancesList.size() == 6) {
					double shortestDist = getShortestDistance(distancesList);

					tvDistanceDuration.setText("Shortest Distance:"
							+ shortestDist + " km");
					allLocationsLoop = false;
				}

			}

			// Drawing polyline in the Google Map for the i-th route
			googleMap.addPolyline(lineOptions);
		}

		private double getShortestDistance(List<Double> distancesList) {
			// Validates input
			if (distancesList == null) {
				throw new IllegalArgumentException("The Array must not be null");
			} else if (distancesList.size() == 0) {
				throw new IllegalArgumentException("Array cannot be empty.");
			}

			// Finds and returns min
			double min = distancesList.get(0);
			for (int i = 1; i < distancesList.size(); i++) {
				if (Double.isNaN(distancesList.get(i))) {
					return Double.NaN;
				}
				if (distancesList.get(i) < min) {
					min = distancesList.get(i);
				}
			}

			return min;
		}

		private void drawDisplacement() {
			Location loc1 = new Location("I am here:");
			loc1.setLatitude(currentLatLng.latitude);
			loc1.setLongitude(currentLatLng.longitude);

			Location loc2 = new Location("Dest:");
			loc2.setLatitude(currentDestLatLng.latitude);
			loc2.setLongitude(currentDestLatLng.longitude);

			double distance = (loc1.distanceTo(loc2)) / 1000;
			tvDistanceDuration.setText("Distace:" + Double.toString(distance));

			PolylineOptions lineOptions = new PolylineOptions();
			lineOptions.add(currentLatLng);
			lineOptions.add(currentDestLatLng);
			lineOptions.width(5);
			lineOptions.color(Color.RED);

			// Drawing polyline in the Google Map for the i-th route
			googleMap.addPolyline(lineOptions);

		}
	}

	public OnCameraChangeListener getCameraChangeListener() {
		return new OnCameraChangeListener() {
			@Override
			public void onCameraChange(CameraPosition position) {
				Log.d("Zoom", "Zoom: " + position.zoom);

				if (previousZoomLevel != position.zoom) {
					isZooming = true;
				}

				previousZoomLevel = position.zoom;
			}
		};
	}

	@Override
	public void onProviderDisabled(String provider) {

	}

	@Override
	public void onProviderEnabled(String provider) {

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.backBtnID:
			finish();
			break;

		case R.id.roadContactNumberTVID:
			try {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(getResources().getString(
						R.string.my_roadassist_link_text)));
				startActivity(i);
				// call(numberTV.getText().toString());
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
					e.printStackTrace();
				}
			}
			break;

		default:
			break;
		}
	}

	private void call(String string) {
		showDialog(CALL_NUMBER);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			if (id == 1) {
				AlertDialog callOneDialog = null;
				switch (id) {
				case CALL_NUMBER:
					LayoutInflater callInflater1 = LayoutInflater.from(this);
					View callOneView1 = callInflater1.inflate(
							R.layout.dialog_layout_call_one, null);
					AlertDialog.Builder callAlert1 = new AlertDialog.Builder(
							this);
					callAlert1.setView(callOneView1);
					callOneDialog = callAlert1.create();
					break;
				}
				return callOneDialog;
			} else if (id == 1207) {
				AlertDialog GPSAlert = null;
				LayoutInflater liDelete = LayoutInflater
						.from(MyRoadsideAssist.this);
				View deleteFavView = liDelete.inflate(
						R.layout.dialog_layout_delete_favorite, null);
				AlertDialog.Builder adbDeleteFav = new AlertDialog.Builder(
						MyRoadsideAssist.this);
				adbDeleteFav.setView(deleteFavView);
				GPSAlert = adbDeleteFav.create();
				return GPSAlert;
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
		return super.onCreateDialog(id);
	}

	@Override
	protected void onResume() {

		super.onResume();
	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		try {
			if (id == 1) {
				switch (id) {
				case CALL_NUMBER:
					final AlertDialog alertDialog1 = (AlertDialog) dialog;

					TextView alertTitle = (TextView) alertDialog1
							.findViewById(R.id.firstLoginCEPUTitleTVID);
					alertTitle.setTypeface(Utility.font_bold);
					TextView tv11 = (TextView) alertDialog1
							.findViewById(R.id.callNumberOneTVID);
					tv11.setTypeface(Utility.font_reg);
					tv11.setText(numberTV.getText().toString());
					Button yesBtn = (Button) alertDialog1
							.findViewById(R.id.call_one_yesBtnID);
					yesBtn.setTypeface(Utility.font_bold);
					Button noBtn = (Button) alertDialog1
							.findViewById(R.id.call_one_noBtnID);
					noBtn.setTypeface(Utility.font_bold);
					yesBtn.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							try {

								/*
								 * Intent intent = new
								 * Intent(Intent.ACTION_CALL);
								 * intent.setData(Uri.parse("tel:09246537791"));
								 * startActivity(intent);
								 */
								// getApplicationContext().sendBroadcast(intent);
								// sendBroadcast(intent);
								alertDialog1.dismiss();
							} catch (Exception e) {
								Log.w("Hari-->", e);
							}

						}
					});
					noBtn.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							alertDialog1.dismiss();
						}
					});
					break;
				}
			} else if (id == 1207) {
				final AlertDialog alt3 = (AlertDialog) dialog;
				TextView alertTilteTV = (TextView) alt3
						.findViewById(R.id.firstLoginCEPUTitleTVID);
				alertTilteTV.setTypeface(Utility.font_bold);
				alertTilteTV.setText("GPS Settings !");
				TextView tv22 = (TextView) alt3
						.findViewById(R.id.deleteFavTVID);
				tv22.setTypeface(Utility.font_reg);
				tv22.setText("GPS is not enabled. Do you want to go to settings menu?");
				Button deleteFavYesBtn = (Button) alt3
						.findViewById(R.id.delete_fav_yesBtnID);
				deleteFavYesBtn.setTypeface(Utility.font_bold);
				deleteFavYesBtn.setText("Settings");
				Button deleteNoFavBtn = (Button) alt3
						.findViewById(R.id.delete_fav_noBtnID);
				deleteNoFavBtn.setTypeface(Utility.font_bold);
				deleteNoFavBtn.setText("Cancel");
				deleteFavYesBtn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						try {
							Intent intent = new Intent(
									android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
							MyRoadsideAssist.this.startActivity(intent);
							MyRoadsideAssist.this.finish();
						} catch (Exception e) {
							Log.w("Hari-->", e);
						}
						alt3.dismiss();
					}
				});
				deleteNoFavBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						alt3.dismiss();
					}
				});
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onInfoWindowClick(Marker marker) {

		for (int i = 1; i < 7; i++) {

			DecimalFormat dtime = new DecimalFormat("#.###");

			Double val = marker.getPosition().latitude;
			val = Double.valueOf(dtime.format(val));
			Double markerLatValue = Double.valueOf(dtime.format(marker
					.getPosition().latitude));
			Double actualLatValue = Double.valueOf(dtime.format(Double
					.parseDouble((locationsPositions[i].split("/")[0]))));
			
			Double val2 = marker.getPosition().longitude;
			val2 = Double.valueOf(dtime.format(val2));
			Double markerLonValue = Double.valueOf(dtime.format(marker
					.getPosition().longitude));
			Double actualLonValue = Double.valueOf(dtime.format(Double
					.parseDouble((locationsPositions[i].split("/")[1]))));
			
			
			if( (markerLatValue < (actualLatValue + 0.004)
					&& markerLatValue > (actualLatValue - 0.004)) && (markerLonValue < (actualLonValue + 0.004)
							&& markerLonValue > (actualLonValue - 0.004)))
			{
				Intent intent = new Intent(Intent.ACTION_CALL);
				intent.setData(Uri.parse("tel:" + callDataArray[i]));
				startActivity(intent);
			}

		}
	}

	@Override
	public boolean onMarkerClick(Marker arg0) {
		// TODO Auto-generated method stub
		return false;
	}

}
