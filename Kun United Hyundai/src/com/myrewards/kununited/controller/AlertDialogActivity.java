package com.myrewards.kununited.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.myrewards.kununited.utils.Utility;

public class AlertDialogActivity extends Activity {
	private static final int MY_PARKING_TIMER_EXPIRE = 1;
	TextView tv111;
	Button okBtn;
	NewParkingTimeActivity newParkingob;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		newParkingob = new NewParkingTimeActivity();
		Settings.System.putInt(getContentResolver(),
				Settings.System.SOUND_EFFECTS_ENABLED, 1);
		displayAlert();
	}

	private void displayAlert() {
		showDialog(MY_PARKING_TIMER_EXPIRE);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == 1) {
			AlertDialog myParkingExpireDialog = null;
			switch (id) {
			case MY_PARKING_TIMER_EXPIRE:
				LayoutInflater lif = LayoutInflater.from(this);
				View expireView = lif.inflate(
						R.layout.dialog_layout_my_parking_timer_alert, null);
				AlertDialog.Builder adbMyParking = new AlertDialog.Builder(this);
				adbMyParking.setCancelable(false);
				adbMyParking.setView(expireView);
				myParkingExpireDialog = adbMyParking.create();
				break;
			}
			return myParkingExpireDialog;
		}
		return null;
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		if (id == 1) {
			switch (id) {
			case MY_PARKING_TIMER_EXPIRE:
				final AlertDialog alert2 = (AlertDialog) dialog;
				TextView alertTitle=(TextView)alert2.findViewById(R.id.myParkingTimerCEPUTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);
				
				tv111 = (TextView) alert2
						.findViewById(R.id.expire_my_parkingTVID);
				
				tv111.setTypeface(Utility.font_reg);
				
				okBtn = (Button) alert2.findViewById(R.id.expireOKBtnID);
				okBtn.setTypeface(Utility.font_bold);
				alert2.setCancelable(false);
				okBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						alert2.dismiss();
						try {
							NewParkingTimeActivity.ReminderSet = false;
							NewParkingTimeActivity.setTime = 0;
							NewParkingTimeActivity.setTimeBtn.setText("Set Time");
							NewParkingTimeActivity.mAlarmApplication.stopTimer();
						} catch (Exception e) {
							if (e != null) {
								Log.w("Hari-->DEBUG", e);
								e.printStackTrace();
							}
						}
						finish();
					}
				});
				break;
			}
		}
	}
}