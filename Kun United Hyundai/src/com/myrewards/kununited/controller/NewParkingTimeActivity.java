package com.myrewards.kununited.controller;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.NYXDigital.NiceSupportMapFragment;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.myrewards.kununited.timer.SimpleTimerApplication;
import com.myrewards.kununited.timer.TimeConversionUtil;
import com.myrewards.kununited.utils.DatabaseHelper;
import com.myrewards.kununited.utils.Utility;

@SuppressLint("ShowToast")
public class NewParkingTimeActivity extends FragmentActivity implements
		android.location.LocationListener, OnClickListener {
	public double latitude, longitude;
	Button addLocationBTN, backBtn, startTimeBtn, endTimeBtn, setRemindBtn,
			clearRemindBtn;
	public static Button setTimeBtn, scanBarBtn;
	TextView titleTV;
	GoogleMap googleMap;
	private static final int SET_TIME_DIALOG = 1;
	private static final int ADD_CURRENT_LOCATION = 3;
	private static final int SET_REMINDER = 4;
	private static final int SET_REMINDER_ALREADY_SET = 9;
	private static final int SET_REMINDER_PROPERLY = 5;
	private static final int CLEAR_REMINDER = 6;
	private static final int CLEAR_REMINDER_NO = 7;
	private static final int ALREADY_LOCATION_ADDED = 8;
	private static final int SET_REMINDER_DIALOG = 10;
	DatabaseHelper helper;
	public static Boolean countBoolean = false;
	static int count = 0, count2 = 0, timerCount;
	public static Double lat, lang;
	static Marker startPerc;
	public static int startTime, endTime, setTime = 0;
	public static String setTimeText;
	public Handler hand;
	AlarmManager al;
	static Boolean ReminderSet = false;
	// add current Location Variables
	TextView tv11, tv12, tv13, tv14, tv15;
	Button yesBtn;
	Button noBtn;

	public static int hourA = 0, minuteA = 0, hourB = 0, minuteB = 0;
	public int alarmcount = 0;

	// these all for the timer
	public static final String ALARM_TIME = "alarmkey";
	public static final String ALARM_NAME = "alarmnamekey";
	public static final int TIME_MAX_LENGTH = 6;

	public TextView mTimeView;

	public static SimpleTimerApplication mAlarmApplication;
	public CountDownTimer mCountDownTimer;
	public boolean mCountingDown;
	Location location;
	String provider;
	GPSTracker mGPS = null;

	//Initialize to a non-valid zoom value
			private float previousZoomLevel = -1.0f;
			private boolean isZooming = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (PreferenceManager.getDefaultSharedPreferences(
				getApplicationContext()).getBoolean(
				getString(R.string.key_button_placement), true)) {
			setContentView(R.layout.my_parking_time);
		} else {
			setContentView(R.layout.my_parking_time);
		}

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		titleTV = (TextView) findViewById(R.id.titleTVID);
		titleTV.setTypeface(Utility.font_bold);
		titleTV.setText(getResources().getString(R.string.my_parking_time));

		scanBarBtn = (Button) findViewById(R.id.scanBtnID);
		scanBarBtn.setVisibility(View.GONE);

		backBtn = (Button) findViewById(R.id.backBtnID);
		backBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		backBtn.setOnClickListener(this);
		helper = new DatabaseHelper(this);
		addLocationBTN = (Button) findViewById(R.id.addLocationBtnID);
		addLocationBTN.setTypeface(Utility.font_bold);
		setTimeBtn = (Button) findViewById(R.id.setTimeBtnID);
		setTimeBtn.setTypeface(Utility.font_bold);
		setRemindBtn = (Button) findViewById(R.id.setReminderBtnID);
		setRemindBtn.setTypeface(Utility.font_bold);
		clearRemindBtn = (Button) findViewById(R.id.clearReminderBtnID);
		clearRemindBtn.setTypeface(Utility.font_bold);

		setTimeBtn.setOnClickListener(this);
		setRemindBtn.setOnClickListener(this);
		clearRemindBtn.setOnClickListener(this);

		if (ReminderSet == true && countBoolean == true) {
			addLocationBTN.setText("Clear Location");
		} else {
			addLocationBTN.setText("Add Location");
		}

		if (setTime > 0 && ReminderSet == true) {
			if (setTime == 1) {
				setTimeBtn.setText("Set Time is: " + setTime + " minute");
				mCountingDown = true;
			} else
				setTimeBtn.setText("Set Time is: " + setTime + " minutes");
		}

		try {
			initilizeMaps();
		} catch (Exception e) {
			Log.w("Hari--->", e);
		}

		addLocationBTN.setOnClickListener(this);

		// this is for timer activity
		mAlarmApplication = (SimpleTimerApplication) getApplicationContext();
		if (!(setTime > 0 && ReminderSet == true)) {
			mCountingDown = false;
		}
		restoreText();
		if (ReminderSet == false && !(setTime > 0)) {
			// Stop timer when app is launched if timer is not active
			mAlarmApplication.stopTimer();
		}

		if (ReminderSet == true && countBoolean == true) {
			if (location != null) {
				onLocationChanged(location);
				showAddedLocation();
			} else {
				try {
					// Getting LocationManager object from System Service
					// LOCATION_SERVICE
					LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
					// Creating a criteria object to retrieve provider
					Criteria criteria = new Criteria();
					// Getting the name of the best provider
					if (locationManager != null) {
						provider = locationManager.getBestProvider(criteria, true);
					}
					if (provider != null) {
						location = locationManager.getLastKnownLocation(provider);
					}
					if (location != null) {
						onLocationChanged(location);
						showAddedLocation();
					}
				} catch (Exception e) {
					Log.w("Hari-->", e);
				}
			}
		}
	}

	private void initilizeMaps() {
		if (googleMap == null) {
			NiceSupportMapFragment mapFragment = (NiceSupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.mapfragmentId);
			googleMap = mapFragment.getMap();
			mapFragment.setPreventParentScrolling(false);
		}

		// check if map is created successfully or not
		if (googleMap == null) {
			Toast.makeText(getApplicationContext(),
					"Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
		} else {
			googleMap.setMyLocationEnabled(true);
			if (mGPS == null) {
				mGPS = new GPSTracker(NewParkingTimeActivity.this);
			}

			// check if mGPS object is created or not
			if (mGPS != null && location == null) {
				location = mGPS.getLocation();
			}

			// check if location is created or not
        	if (location != null) {
        		LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

				CameraPosition cameraPosition = new CameraPosition.Builder()
						.target(latLng) // Sets the center of the map to Mountain View
						.zoom(12) // Sets the zoom
						.bearing(90) // Sets the orientation of the camera to east
						.tilt(30) // Sets the tilt of the camera to 30 degrees
						.build(); // Creates a CameraPosition from the builder
        		// Showing the current location in Google Map
				CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 12);
				
				// Showing the current location in Google Map
				googleMap.moveCamera(cameraUpdate);

				// Zoom in the Google Map
				googleMap.animateCamera(cameraUpdate);
				// Zoom in the Google Map
			//	googleMap.animateCamera(CameraUpdateFactory.zoomTo(14));
    			onLocationChanged(location);
    		}      	
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (ReminderSet == false) {
				count = 0;
				countBoolean = false;
				count2 = 0;
			}

			finish();
		}
		return true;

	}

	@SuppressWarnings("unused")
	@Override
	public void onLocationChanged(Location location) {
		try {

			String str1 = null;

			// Getting latitude of the current location
			double latitude = location.getLatitude();

			// Getting longitude of the current location
			double longitude = location.getLongitude();
			if (count == 0 && countBoolean == false) {
				lat = latitude;
				lang = longitude;

			}
			if (count2 == 1) {
				LatLng yyy = new LatLng(lat, lang);
				startPerc = googleMap
						.addMarker(new MarkerOptions()
								.position(yyy)
								.title("I am Here")
								.snippet("Me")
								.icon(BitmapDescriptorFactory
										.fromResource(R.drawable.park)));
			}
			// Creating a LatLng object for the current location
			LatLng latLng = new LatLng(latitude, longitude);

			CameraPosition cameraPosition = new CameraPosition.Builder()
					.target(latLng) // Sets the center of the map to Mountain View
					.zoom(12) // Sets the zoom
					.bearing(90) // Sets the orientation of the camera to east
					.tilt(30) // Sets the tilt of the camera to 30 degrees
					.build(); // Creates a CameraPosition from the builder

			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 12);
			
			// Showing the current location in Google Map
			googleMap.moveCamera(cameraUpdate);

			// Zoom in the Google Map
			googleMap.animateCamera(cameraUpdate);
			// Zoom in the Google Map
		//	googleMap.animateCamera(CameraUpdateFactory.zoomTo(14));
			
			googleMap.setOnCameraChangeListener(getCameraChangeListener());
			Geocoder geo = new Geocoder(
					NewParkingTimeActivity.this.getApplicationContext(),
					Locale.getDefault());
			try {
				List<Address> ads = geo.getFromLocation(latitude, longitude, 1);
				str1 = ads.get(0).getSubLocality() + ", "
						+ ads.get(0).getLocality();
			} catch (IOException e) {
				e.printStackTrace();
			}

			Marker startPerc = googleMap.addMarker(new MarkerOptions()
					.position(latLng).title("Start").snippet("Me")
					.icon(BitmapDescriptorFactory.fromResource(R.drawable.malls)));
			startPerc.remove();
		} catch (Exception e) {
			Log.w("Hari-->", e);
		}
	}
	public OnCameraChangeListener getCameraChangeListener()
	{
	    return new OnCameraChangeListener() 
	    {
	        @Override
	        public void onCameraChange(CameraPosition position) 
	        {
	            Log.d("Zoom", "Zoom: " + position.zoom);

	            if(previousZoomLevel != position.zoom)
	            {
	                isZooming = true;
	            }

	            previousZoomLevel = position.zoom;
	        }
	    };
	}
	@Override
	public void onProviderDisabled(String provider) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.addLocationBtnID:
			try {
				if (count == 0 && countBoolean == false)
					showDialog(ADD_CURRENT_LOCATION);
				else if (countBoolean == true) {
					showDialog(ALREADY_LOCATION_ADDED);
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
					e.printStackTrace();
				}
			}
			break;
		case R.id.backBtnID:
			try {
				if (ReminderSet == false) {
					count = 0;
					countBoolean = false;
					count2 = 0;
				}
				finish();
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
					e.printStackTrace();
				}
			}
			break;
		case R.id.setTimeBtnID:
			//if (setTime > 0 && ReminderSet == true) {
			//	Toast.makeText(getApplicationContext(), "Timer is already set.", 3000).show();
			//} else{
				showDialog(SET_TIME_DIALOG);
			//}
			break;
		case R.id.setReminderBtnID:
			try {
				if (setTime > 0) {
					if (ReminderSet == false) {
						showDialog(SET_REMINDER);
					} else {
						showDialog(SET_REMINDER_ALREADY_SET);
					}
				} else {
					showDialog(SET_REMINDER_PROPERLY);
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
					e.printStackTrace();
				}
			}
			break;
		case R.id.clearReminderBtnID:
			try {
				if (setTime > 0) {
					if (ReminderSet == true) {
						showDialog(CLEAR_REMINDER);
					} else {
						showDialog(CLEAR_REMINDER_NO);
					}
				} else {
					showDialog(CLEAR_REMINDER_NO);
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
					e.printStackTrace();
				}
			}
			break;
		default:
			break;
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		try {

			if (id == 1) {
				return new TimePickerDialog(this, new TimeSet1(), 00, 00, true);
			} else if (id == 2) {
				return new TimePickerDialog(this, new TimeSet2(), 00, 00, true);
			} else if (id == 3) {
				AlertDialog addLocationDialog = null;
				switch (id) {
				case ADD_CURRENT_LOCATION:
					LayoutInflater mli = LayoutInflater.from(this);
					View allFieldsView = mli.inflate(
							R.layout.dialog_parking_add_location, null);
					AlertDialog.Builder allFalert = new AlertDialog.Builder(this);
					allFalert.setCancelable(false);
					allFalert.setView(allFieldsView);
					addLocationDialog = allFalert.create();
					break;
				}
				return addLocationDialog;
			} else if (id == 4) {
				AlertDialog setReminderDialog = null;
				switch (id) {
				case SET_REMINDER:
					LayoutInflater mli = LayoutInflater.from(this);
					View allFieldsView = mli.inflate(
							R.layout.dialog_parking_set_reminder_one, null);
					AlertDialog.Builder setReminderAlert = new AlertDialog.Builder(
							this);
					setReminderAlert.setCancelable(false);
					setReminderAlert.setView(allFieldsView);
					setReminderDialog = setReminderAlert.create();
					break;
				}
				return setReminderDialog;
			} else if (id == 5) {
				AlertDialog alreadySetReminderDialog = null;
				switch (id) {
				case SET_REMINDER_PROPERLY:
					LayoutInflater mli = LayoutInflater.from(this);
					View allFieldsView = mli.inflate(
							R.layout.dialog_layout_set_remainder_properly, null);
					AlertDialog.Builder setReminderAlert = new AlertDialog.Builder(
							this);
					setReminderAlert.setCancelable(false);
					setReminderAlert.setView(allFieldsView);
					alreadySetReminderDialog = setReminderAlert.create();
					break;
				}
				return alreadySetReminderDialog;
			} else if (id == 6) {
				AlertDialog clearReminderDialog = null;
				switch (id) {
				case CLEAR_REMINDER:
					LayoutInflater mli = LayoutInflater.from(this);
					View allFieldsView = mli
							.inflate(
									R.layout.dialog_parking_timer_clear_remainder_one,
									null);
					AlertDialog.Builder clearReminderAlert = new AlertDialog.Builder(
							this);
					clearReminderAlert.setCancelable(false);
					clearReminderAlert.setView(allFieldsView);
					clearReminderDialog = clearReminderAlert.create();
					break;
				}
				return clearReminderDialog;
			} else if (id == 7) {
				AlertDialog clearReminderDialog = null;
				switch (id) {
				case CLEAR_REMINDER_NO:
					LayoutInflater mli = LayoutInflater.from(this);
					View allFieldsView = mli.inflate(
							R.layout.dialog_parking_clear_reminder_no_reminder,
							null);
					AlertDialog.Builder clearReminderAlert = new AlertDialog.Builder(
							this);
					clearReminderAlert.setCancelable(false);
					clearReminderAlert.setView(allFieldsView);
					clearReminderDialog = clearReminderAlert.create();
					break;
				}
				return clearReminderDialog;
			} else if (id == 8) {
				AlertDialog clearReminderDialog = null;
				switch (id) {
				case ALREADY_LOCATION_ADDED:
					LayoutInflater mli = LayoutInflater.from(this);
					View allFieldsView = mli.inflate(
							R.layout.dialog_parking_add_location_already, null);
					AlertDialog.Builder clearReminderAlert = new AlertDialog.Builder(
							this);
					clearReminderAlert.setCancelable(false);
					clearReminderAlert.setView(allFieldsView);
					clearReminderDialog = clearReminderAlert.create();
					break;
				}
				return clearReminderDialog;
			} else if (id == 9) {
				AlertDialog clearReminderDialog = null;
				switch (id) {
				case SET_REMINDER_ALREADY_SET:
					LayoutInflater mli = LayoutInflater.from(this);
					View allFieldsView = mli.inflate(
							R.layout.dialog_parking_timer_set_already, null);
					AlertDialog.Builder clearReminderAlert = new AlertDialog.Builder(
							this);
					clearReminderAlert.setCancelable(false);
					clearReminderAlert.setView(allFieldsView);
					clearReminderDialog = clearReminderAlert.create();
					break;
				}
				return clearReminderDialog;
			} else if (id == 10) {
				AlertDialog reminder_notification_dialog = null;
				switch (id) {
				case SET_REMINDER_DIALOG:
					LayoutInflater mli = LayoutInflater.from(this);
					View allFieldsView = mli
							.inflate(
									R.layout.dialog_parking_timer_clear_remainder_two,
									null);
					AlertDialog.Builder clearReminderAlert = new AlertDialog.Builder(
							this);
					clearReminderAlert.setCancelable(false);
					clearReminderAlert.setView(allFieldsView);
					reminder_notification_dialog = clearReminderAlert.create();
					break;
				}
				return reminder_notification_dialog;
			} else if (id == 1207) {
				AlertDialog GPSAlert = null;
				LayoutInflater liDelete = LayoutInflater
						.from(NewParkingTimeActivity.this);
				View deleteFavView = liDelete.inflate(
						R.layout.dialog_layout_delete_favorite, null);
				AlertDialog.Builder adbDeleteFav = new AlertDialog.Builder(
						NewParkingTimeActivity.this);
				adbDeleteFav.setView(deleteFavView);
				GPSAlert = adbDeleteFav.create();
				return GPSAlert;
			}
		
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
		return super.onCreateDialog(id);
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		try {
			if (id == 3) {
				final AlertDialog alertDialog1 = (AlertDialog) dialog;
				TextView alertTitle = (TextView) alertDialog1
						.findViewById(R.id.firstLoginCEPUTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);
				tv11 = (TextView) alertDialog1
						.findViewById(R.id.addLocationdialogTVID);
				tv11.setTypeface(Utility.font_reg);

				Button yesBtn = (Button) alertDialog1
						.findViewById(R.id.add_location_dialog_yesBtnID);
				yesBtn.setTypeface(Utility.font_bold);
				Button noBtn = (Button) alertDialog1
						.findViewById(R.id.add_location_dialog_noBtnID);
				noBtn.setTypeface(Utility.font_bold);
				alertDialog1.setCancelable(false);

				yesBtn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						try {
							if (googleMap != null) {
								if (mGPS == null) {
									mGPS = new GPSTracker(NewParkingTimeActivity.this);
								}

								// check if mGPS object is created or not
								if (mGPS != null) {
									// if(mGPS.canGetLocation()){
									if (location == null) {
										location = mGPS.getLocation();
									}

									if (location != null) {
										onLocationChanged(location);
										count2 = 1;
										showAddedLocation();
										count++;
										countBoolean = true;
										alertDialog1.dismiss();
										addLocationBTN.setText("Clear Location");
										// }
									} else {
										try {
											showDialog(1207);
											alertDialog1.dismiss();
											// mGPS.showSettingsAlert();
										} catch (Exception e) {
											Log.w("Hari--->", e);
										}
									}
								}
							}
						} catch (Exception e) {
							Log.w("Hari-->", e);
						}
					}
				});
				noBtn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog1.dismiss();
					}
				});
			} else if (id == 4) {
				switch (id) {
				case SET_REMINDER:
					final AlertDialog alertDialog2 = (AlertDialog) dialog;
					TextView alertTitle = (TextView) alertDialog2
							.findViewById(R.id.setReminderCEPUTitleTVID);
					alertTitle.setTypeface(Utility.font_bold);
					tv12 = (TextView) alertDialog2
							.findViewById(R.id.setReminder_one_dialogTVID);
					tv12.setTypeface(Utility.font_reg);

					Button yesBtn = (Button) alertDialog2
							.findViewById(R.id.set_reminder_one_dialog_yesBtnID);
					yesBtn.setTypeface(Utility.font_bold);
					Button noBtn = (Button) alertDialog2
							.findViewById(R.id.set_reminder_one_dialog_noBtnID);
					noBtn.setTypeface(Utility.font_bold);
					alertDialog2.setCancelable(false);

					yesBtn.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							// setReminder();
							ReminderSet = true;
							showDialog(SET_REMINDER_DIALOG);
							alertDialog2.dismiss();

						}
					});
					noBtn.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							setTimeBtn.setText("Set Time");
							alertDialog2.dismiss();
						}
					});
					break;
				}
			} else if (id == 5) {
				switch (id) {
				case SET_REMINDER_PROPERLY:
					final AlertDialog alertDialog1 = (AlertDialog) dialog;

					TextView alertTitle = (TextView) alertDialog1
							.findViewById(R.id.myParkingTimerCEPUTitleTVID);

					alertTitle.setTypeface(Utility.font_bold);

					tv13 = (TextView) alertDialog1
							.findViewById(R.id.properly_set_my_parkingTVID);
					tv13.setTypeface(Utility.font_reg);

					Button okbutton = (Button) alertDialog1
							.findViewById(R.id.properlyOKBtnID);
					okbutton.setTypeface(Utility.font_bold);
					alertDialog1.setCancelable(false);

					okbutton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							setTimeBtn.setText("Set Time");
							alertDialog1.dismiss();
						}
					});
					break;
				}
			} else if (id == 6) {
				switch (id) {
				case CLEAR_REMINDER:
					final AlertDialog alertDialog2 = (AlertDialog) dialog;

					TextView alertTitle = (TextView) alertDialog2
							.findViewById(R.id.firstLoginCEPUTitleTVID);

					alertTitle.setTypeface(Utility.font_bold);

					tv14 = (TextView) alertDialog2
							.findViewById(R.id.clearRemindersdialogTVID);
					tv14.setTypeface(Utility.font_reg);

					Button yesBtn = (Button) alertDialog2
							.findViewById(R.id.clear_reminder_dialog_yesBtnID);
					yesBtn.setTypeface(Utility.font_bold);
					Button noBtn = (Button) alertDialog2
							.findViewById(R.id.clear_reminder_dialog_noBtnID);
					noBtn.setTypeface(Utility.font_bold);
					alertDialog2.setCancelable(false);

					yesBtn.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							ReminderSet = false;
							clearReminder();
							alertDialog2.dismiss();
							mAlarmApplication.setTimeString("");
							mTimeView.setText(R.string.default_time);
							mAlarmApplication.stopTimer();
							stopTextCountdown();
							mCountingDown = false;
						}
					});
					noBtn.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							alertDialog2.dismiss();
						}
					});
					break;
				}
			} else if (id == 7) {
				switch (id) {
				case CLEAR_REMINDER_NO:
					final AlertDialog alertDialog1 = (AlertDialog) dialog;

					TextView alertTitle = (TextView) alertDialog1
							.findViewById(R.id.myParkingTimerCEPUTitleTVID);

					alertTitle.setTypeface(Utility.font_bold);

					tv15 = (TextView) alertDialog1
							.findViewById(R.id.no_reminders_set_my_parkingTVID);

					tv15.setTypeface(Utility.font_reg);

					Button okbutton = (Button) alertDialog1
							.findViewById(R.id.no_remindersOKBtnID);
					okbutton.setTypeface(Utility.font_bold);
					alertDialog1.setCancelable(false);
					okbutton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							alertDialog1.dismiss();
						}
					});
					break;
				}
			} else if (id == 8) {
				switch (id) {
				case ALREADY_LOCATION_ADDED:
					final AlertDialog alertDialog2 = (AlertDialog) dialog;

					TextView alertTitle = (TextView) alertDialog2
							.findViewById(R.id.firstLoginCEPUTitleTVID);

					alertTitle.setTypeface(Utility.font_bold);

					tv14 = (TextView) alertDialog2
							.findViewById(R.id.addLocAlreadyTVID);

					tv14.setTypeface(Utility.font_reg);

					Button yesBtn = (Button) alertDialog2
							.findViewById(R.id.add_loc_already_yesBtnID);
					yesBtn.setTypeface(Utility.font_bold);
					Button noBtn = (Button) alertDialog2
							.findViewById(R.id.add_loc_already_noBtnID);
					noBtn.setTypeface(Utility.font_bold);
					alertDialog2.setCancelable(false);

					yesBtn.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							count = 0;
							countBoolean = false;
							count2 = 0;
							startPerc.remove();
							googleMap.clear();
							addLocationBTN.setText("Add Location");

							mCountingDown = true;
							alertDialog2.dismiss();
						}
					});
					noBtn.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							alertDialog2.dismiss();
						}
					});
					break;
				}
			} else if (id == 9) {
				switch (id) {
				case SET_REMINDER_ALREADY_SET:
					final AlertDialog alertDialog1 = (AlertDialog) dialog;

					TextView alertTitle = (TextView) alertDialog1
							.findViewById(R.id.myParkingTimerCEPUTitleTVID);

					alertTitle.setTypeface(Utility.font_bold);

					tv15 = (TextView) alertDialog1
							.findViewById(R.id.already_set_my_parkingTVID);

					tv15.setTypeface(Utility.font_reg);

					Button okbutton = (Button) alertDialog1
							.findViewById(R.id.alreadyOKBtnID);
					okbutton.setTypeface(Utility.font_bold);
					alertDialog1.setCancelable(false);

					okbutton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							alertDialog1.dismiss();
						}
					});
					break;
				}
			}

			else if (id == 10) {
				switch (id) {
				case SET_REMINDER_DIALOG:
					final AlertDialog alertDialog1 = (AlertDialog) dialog;

					TextView alertTitle = (TextView) alertDialog1
							.findViewById(R.id.myParkingTimerCEPUTitleTVID);

					alertTitle.setTypeface(Utility.font_bold);

					tv15 = (TextView) alertDialog1
							.findViewById(R.id.already_set_my_parkingTVID);
					tv15.setTypeface(Utility.font_reg);

					Button okbutton = (Button) alertDialog1
							.findViewById(R.id.alreadyOKBtnID);
					okbutton.setTypeface(Utility.font_bold);
					tv15.setText("Alarm has been set and this will remind you after "
							+ setTime + " minutes");
					alertDialog1.setCancelable(false);

					okbutton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {

							// this is for timer

							try {

								if (!mCountingDown) {
									if (mAlarmApplication.getTimeString().length() < TIME_MAX_LENGTH) {
										mAlarmApplication
												.appendToTimeString(((Button) v)
														.getText().toString());
										updateTimeView();
									} else {
										Toast.makeText(getApplicationContext(),
												R.string.time_too_long_warning,
												Toast.LENGTH_SHORT).show();
									}
								}

								Alarm();
								mAlarmApplication.startTimer(((setTime) * (60000)));
								stopTextCountdown();
								startTextCountdown();
								mAlarmApplication.setTimeString("");
								mCountingDown = true;
							} catch (Exception e) {

							}
							alertDialog1.dismiss();
						}
					});
					break;
				}
			} else if (id == 1207) {
				final AlertDialog alt3 = (AlertDialog) dialog;
				TextView alertTilteTV = (TextView) alt3
						.findViewById(R.id.firstLoginCEPUTitleTVID);
				alertTilteTV.setTypeface(Utility.font_bold);
				alertTilteTV.setText("GPS Settings !");
				TextView tv22 = (TextView) alt3.findViewById(R.id.deleteFavTVID);
				tv22.setTypeface(Utility.font_reg);
				tv22.setText("GPS is not enabled. Do you want to go to settings menu?");
				Button deleteFavYesBtn = (Button) alt3
						.findViewById(R.id.delete_fav_yesBtnID);
				deleteFavYesBtn.setTypeface(Utility.font_bold);
				deleteFavYesBtn.setText("Settings");
				Button deleteNoFavBtn = (Button) alt3
						.findViewById(R.id.delete_fav_noBtnID);
				deleteNoFavBtn.setTypeface(Utility.font_bold);
				deleteNoFavBtn.setText("Cancel");
				deleteFavYesBtn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						try {
							Intent intent = new Intent(
									android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
							NewParkingTimeActivity.this.startActivity(intent);
						} catch (Exception e) {
							Log.w("Hari-->", e);
						}
						alt3.dismiss();
					}
				});
				deleteNoFavBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						alt3.dismiss();
					}
				});
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
	}

	private void showAddedLocation() {
		try {
			LatLng xxx = new LatLng(lat, lang);
			Geocoder coder = new Geocoder(this, Locale.getDefault());
			List<Address> address = null;
			try {
				address = coder.getFromLocation(lat, lang, 1);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				startPerc = googleMap
						.addMarker(new MarkerOptions()
								.position(xxx)
								.title("I am Here")
								.snippet(address.get(0).getSubLocality())
								.icon(BitmapDescriptorFactory
										.fromResource(R.drawable.park)));
				CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(xxx, 12);
				
				// Showing the current location in Google Map
				googleMap.moveCamera(cameraUpdate);

				// Zoom in the Google Map
				googleMap.animateCamera(cameraUpdate);
			} catch (Exception e) {
				startPerc = googleMap
						.addMarker(new MarkerOptions()
								.position(xxx)
								.title("I am Here")
								.icon(BitmapDescriptorFactory
										.fromResource(R.drawable.park)));
				CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(xxx, 12);
				
				// Showing the current location in Google Map
				googleMap.moveCamera(cameraUpdate);

				// Zoom in the Google Map
				googleMap.animateCamera(cameraUpdate);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
	}

	class TimeSet1 implements OnTimeSetListener {
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			setTime = (hourOfDay * 60) + minute;
			setTimeBtn.setText("Set Time: " + Integer.toString(hourOfDay) + ":"
					+ Integer.toString(minute));
			setTimeText = Integer.toString(hourOfDay) + ":"
					+ Integer.toString(hourOfDay);
			hourA = hourOfDay;
			minuteA = minute;
		}
	}

	class TimeSet2 implements OnTimeSetListener {

		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

			endTime = (hourOfDay * 60) + minute;
			endTimeBtn.setText("start time is:" + Integer.toString(hourOfDay)
					+ ":" + Integer.toString(minute));
			hourB = hourOfDay;
			minuteB = minute;
		}
	}

	public void Alarm() {
		try {
			hand = new Handler();
			Calendar cal = Calendar.getInstance();
			Intent in = new Intent(NewParkingTimeActivity.this, Alarm.class);
			PendingIntent pin = PendingIntent.getBroadcast(
					NewParkingTimeActivity.this, 0, in, 0);
			al = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
			al.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis()
					+ ((setTime) * (60000)), pin);
		} catch (Exception e) {

		}
	}

	public void clearReminder() {
		setTimeBtn.setText("Set Time");
		// endTimeBtn.setText("End Time");
		setTime = 0;
		cancelAlarm();
	}

	private void cancelAlarm() {
		AlarmManager al = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		Intent in = new Intent(NewParkingTimeActivity.this, Alarm.class);
		PendingIntent pin = PendingIntent.getBroadcast(
				NewParkingTimeActivity.this, 0, in, 0);
		al.cancel(pin);
	}

	// here below code is for timer activity

	public void restoreText() {
		try {

			SharedPreferences settings = getPreferences(0);

			if (settings != null) {
				long milliseconds = settings.getLong(ALARM_TIME, 0);
				Calendar c = Calendar.getInstance();
				c.setTimeInMillis(milliseconds);
				mAlarmApplication.setCurrentAlarmCalendar(c);
				String alarmName = settings.getString(ALARM_NAME, "");
				mAlarmApplication.setAlarmName(alarmName);
			}

		} catch (Exception e) {

		}
	}

	public void updateTimeView() {

	}

	@Override
	public void onPause() {
		super.onPause();
		try {
			SharedPreferences settings = getPreferences(0);
			SharedPreferences.Editor editor = settings.edit();
			editor.commit();
		} catch (Exception e) {

		}

	}

	@Override
	public void onStop() {
		super.onStop();
		try {
			SharedPreferences settings = getPreferences(0);
			SharedPreferences.Editor editor = settings.edit();
			editor.putLong(ALARM_TIME, mAlarmApplication
					.getCurrentAlarmCalendar().getTimeInMillis());
			editor.commit();
		} catch (Exception e) {

		}
	}

	public void startTextCountdown() {

		try {

			Calendar c = mAlarmApplication.getCurrentAlarmCalendar();
			if (c != null && c.getTimeInMillis() != 0) {
				long alarmTime = c.getTimeInMillis();
				long currentTime = Calendar.getInstance().getTimeInMillis();
				long timeDifference = alarmTime - currentTime;
				if (timeDifference > 0) {
					mCountingDown = true;
				} else {
					mCountingDown = false;
				}
				mCountDownTimer = new CountDownTimer(timeDifference, 1000) {

					@Override
					public void onTick(long millisUntilFinished) {
						mTimeView
								.setText(TimeConversionUtil
										.getTimeStringFromMilliseconds(millisUntilFinished));
					}

					@Override
					public void onFinish() {
						mTimeView.setText(R.string.default_time);
						mCountingDown = false;
					}
				};
				mCountDownTimer.start();
			} else {
				mCountingDown = false;
			}

		} catch (Exception e) {

		}
	}

	public void stopTextCountdown() {
		if (mCountDownTimer != null) {
			mCountDownTimer.cancel();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		// this is for timer

		try {

			mTimeView = (TextView) findViewById(R.id.timerTextView);

			if (mCountDownTimer != null) {
				mCountDownTimer.cancel();
			}
			startTextCountdown();
			if (!mCountingDown && ReminderSet == false && !(setTime > 0)) {
				// Stop timer when app is launched if timer is not active
				mAlarmApplication.stopTimer();
			}
		} catch (Exception e) {

		}
	}
}
