package com.myrewards.kununited.controller;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.kununited.model.User;
import com.myrewards.kununited.service.BayfordServiceListener;
import com.myrewards.kununited.service.GrabItNowService;
import com.myrewards.kununited.utils.DatabaseHelper;
import com.myrewards.kununited.utils.Utility;

public class SendAFriendNewActivity extends Activity implements
		OnClickListener, BayfordServiceListener, OnLongClickListener {
	Button sendBtn11;
	Button backBtn, sendBtn, scanBarBtn;
	TextView titleTV;
	LayoutInflater inflater;
	EditText mUname, mEmail;
	DatabaseHelper helper;
	ListView emailList;
	private static final int DIALOG_SEND = 1;
	private static final int DIALOG_SEND_SUCCESS = 2;
	private static final int DIALOG_SEND_FAILED = 3;
	private static final int DIALOG_SEND_FIELDS_ERROR = 4;
	private static final int DIALOG_SEND_INVALID_EMAIL = 5;
	User user;
	TextView textTV;
	Button okbutton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.send_a_friend);
		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);
		
		titleTV=(TextView)findViewById(R.id.titleTVID);
		titleTV.setTypeface(Utility.font_bold);
		titleTV.setText(getResources().getString(R.string.send_a_friend));
		
		scanBarBtn=(Button)findViewById(R.id.scanBtnID);
		scanBarBtn.setVisibility(View.GONE);	
		
		helper = new DatabaseHelper(this);
		user = new User();
		backBtn = (Button) findViewById(R.id.backBtnID);
		backBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		sendBtn11 = (Button) findViewById(R.id.sendafrndbtnID);
		sendBtn11.getLayoutParams().width=(int)(Utility.screenWidth);
		sendBtn11.getLayoutParams().height=(int)(Utility.screenHeight);
		
		sendBtn11.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		showDialog(DIALOG_SEND);
	}

	@Override
	public void onServiceComplete(Object response, int eventType) {
		try {
			if (response != null) {
				if (response.toString().contains("SUCCESS") || response.toString().contains("success")) 
				{
					showDialog(DIALOG_SEND_SUCCESS);
			} 
			else {
					showDialog(DIALOG_SEND_FAILED);
				}
			}
		
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			if (id == 1) {
				AlertDialog sendDetails = null;
				switch (id) {
				case DIALOG_SEND:
					LayoutInflater inflateSend = LayoutInflater.from(this);
					View deleteFavView = inflateSend.inflate(
							R.layout.dialog_layout_send_emails_frnds, null);
					AlertDialog.Builder adbDeleteFav = new AlertDialog.Builder(this);
					adbDeleteFav.setCancelable(false);
					adbDeleteFav.setView(deleteFavView);
					sendDetails = adbDeleteFav.create();
					break;
				}
				return sendDetails;
			} else if (id == 2) {
				AlertDialog dialogDetails2 = null;
				switch (id) {
				case DIALOG_SEND_SUCCESS:
					LayoutInflater inflater2 = LayoutInflater.from(this);
					View dialogview = inflater2.inflate(
							R.layout.dialog_send_a_frnd_success, null);
					AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(
							this);
					dialogbuilder.setCancelable(false);
					dialogbuilder.setView(dialogview);
					dialogDetails2 = dialogbuilder.create();
					break;
				}
				return dialogDetails2;
			} else if (id == 3) {
				AlertDialog dialogDetails2 = null;
				switch (id) {
				case DIALOG_SEND_FAILED:
					LayoutInflater inflater2 = LayoutInflater.from(this);
					View dialogview = inflater2.inflate(R.layout.dialog_send_a_frnd_failed, null);
					AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
					dialogbuilder.setCancelable(false);
					dialogbuilder.setView(dialogview);
					dialogDetails2 = dialogbuilder.create();
					break;
				}
				return dialogDetails2;
			} else if (id == 4) {
				AlertDialog fieldsDialog = null;
				switch (id) {
				case DIALOG_SEND_FIELDS_ERROR:
					LayoutInflater mli = LayoutInflater.from(this);
					View allFieldsView = mli.inflate(R.layout.dialog_layout_first_login_fields_error, null);
					AlertDialog.Builder allFalert = new AlertDialog.Builder(this);
					allFalert.setCancelable(false);
					allFalert.setView(allFieldsView);
					fieldsDialog = allFalert.create();
					break;
				}
				return fieldsDialog;
			}
			else if (id == 5) {
				AlertDialog fieldsDialog = null;
				switch (id) {
				case DIALOG_SEND_INVALID_EMAIL:
					LayoutInflater mli = LayoutInflater.from(this);
					View allFieldsView = mli.inflate(R.layout.dialog_send_a_frnd_invalid, null);
					AlertDialog.Builder allFalert = new AlertDialog.Builder(this);
					allFalert.setCancelable(false);
					allFalert.setView(allFieldsView);
					fieldsDialog = allFalert.create();
					break;
				}
				return fieldsDialog;
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
		
		return super.onCreateDialog(id);
	}

	@SuppressLint("ShowToast")
	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		try {

			if (id == 1) {
				switch (id) {
				case DIALOG_SEND:
					final AlertDialog alt3 = (AlertDialog) dialog;
					TextView alertTitle = (TextView) alt3.findViewById(R.id.firstLoginCEPUErrorTitleTVID);
					alertTitle.setTypeface(Utility.font_bold);
					mUname = (EditText) alt3.findViewById(R.id.nameId);
					mUname.setTypeface(Utility.font_reg);
					mUname.getLayoutParams().height=(int)(Utility.screenHeight/16.0);
					mEmail = (EditText) alt3.findViewById(R.id.emailId);
					mEmail.setTypeface(Utility.font_reg);
					mEmail.getLayoutParams().height=(int)(Utility.screenHeight/16.0);
					
					final InputMethodManager im = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					im.hideSoftInputFromWindow(mEmail.getWindowToken(), 0);
					mEmail.setOnClickListener(new OnClickListener() {

				        @Override
				        public void onClick(View v) {
				            // TODO Auto-generated method stub
				        im.showSoftInput(mEmail, InputMethodManager.SHOW_IMPLICIT);
				        }
				    });
					
					final InputMethodManager im2 = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					im2.hideSoftInputFromWindow(mUname.getWindowToken(), 0);
					mUname.setOnClickListener(new OnClickListener() {

				        @Override
				        public void onClick(View v) {
				            // TODO Auto-generated method stub
				        im2.showSoftInput(mUname, InputMethodManager.SHOW_IMPLICIT);
				        }
				    });

					mUname.setOnLongClickListener(this);
					mEmail.setOnLongClickListener(this);

					Button sendBtn = (Button) alt3.findViewById(R.id.sendBtnID);
					sendBtn.setTypeface(Utility.font_bold);
					Button sendCloseBtn = (Button) alt3.findViewById(R.id.sendCloseBtnID);
					sendCloseBtn.setTypeface(Utility.font_bold);
					sendCloseBtn.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							im.hideSoftInputFromWindow(mEmail.getWindowToken(), 0);
							im2.hideSoftInputFromWindow(mUname.getWindowToken(), 0);
							alt3.dismiss();
						}
					});
					alt3.setCancelable(false);
					sendBtn.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							String frndName = mUname.getText().toString();
							final String frndEmailID = mEmail.getText().toString();
							if (frndName != null && frndName.trim().length() > 0) {
								if (isEmailValid(frndEmailID)) {
									if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
										GrabItNowService.getGrabItNowService().sendSendAFriendRequest(SendAFriendNewActivity.this,
												Utility.user.getId(),
												mUname.getText().toString(),
												mEmail.getText().toString());
									} else {
										// The Custom Toast Layout Imported here
										LayoutInflater inflater = getLayoutInflater();
										View layout = inflater.inflate(R.layout.toast_no_netowrk,
										(ViewGroup) findViewById(R.id.custom_toast_layout_id));
										//layout.getBackground().setAlpha(128);  // 50% transparent
										 
										// The actual toast generated here.
										Toast toast = new Toast(getApplicationContext());
										toast.setDuration(Toast.LENGTH_LONG);
										toast.setView(layout);
										toast.show();
										//showDialog(NO_NETWORK_CON);
									}
									alt3.dismiss();
								}
								else{
									showDialog(DIALOG_SEND_INVALID_EMAIL);
								}
								
							} else {
								showDialog(DIALOG_SEND_FIELDS_ERROR);
							}
							
							im.hideSoftInputFromWindow(mEmail.getWindowToken(), 0);
							im2.hideSoftInputFromWindow(mUname.getWindowToken(), 0);
						}
					});
					break;
				}
			}
			 else if (id == 2) {
						final AlertDialog alertDialog2 = (AlertDialog) dialog;
						TextView alertTitle = (TextView) alertDialog2
								.findViewById(R.id.firstLoginCEPUTitleTVID);
						
						alertTitle.setTypeface(Utility.font_bold);
						textTV = (TextView) alertDialog2.findViewById(R.id.succSentTVID);
						textTV.setTypeface(Utility.font_reg);
						okbutton = (Button) alertDialog2
								.findViewById(R.id.sendSuccOkBtnID);
						okbutton.setTypeface(Utility.font_bold);
						alertDialog2.setCancelable(false);
						okbutton.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								SendAFriendNewActivity.this.finish();
								alertDialog2.dismiss();
							}
						});
				}
			 else if (id == 3) {
					final AlertDialog alertDialog2 = (AlertDialog) dialog;
					TextView alertTitle = (TextView) alertDialog2
							.findViewById(R.id.alertSucTitleTVID);
					
					alertTitle.setTypeface(Utility.font_bold);
					textTV = (TextView) alertDialog2.findViewById(R.id.failedSentTVID);
					textTV.setTypeface(Utility.font_reg);
					okbutton = (Button) alertDialog2
							.findViewById(R.id.sendFailedOkBtnID);
					okbutton.setTypeface(Utility.font_bold);
					alertDialog2.setCancelable(false);
					okbutton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							SendAFriendNewActivity.this.finish();
							alertDialog2.dismiss();
						}
					});
			} else if (id == 4) {
					final AlertDialog alertDialog1 = (AlertDialog) dialog;
					TextView alertTitle = (TextView) alertDialog1
							.findViewById(R.id.firstLoginCEPUErrorTitleTVID);
					
					alertTitle.setTypeface(Utility.font_bold);
					textTV = (TextView) alertDialog1
							.findViewById(R.id.allFieldsTVID);
					textTV.setTypeface(Utility.font_reg);
					Button okbutton = (Button) alertDialog1
							.findViewById(R.id.allFieldsOKBtnID);
					okbutton.setTypeface(Utility.font_bold);
					alertDialog1.setCancelable(false);
					okbutton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							alertDialog1.dismiss();
						}
					});
			} 
			else if (id == 5) {
				final AlertDialog alertDialog1 = (AlertDialog) dialog;
				TextView alertTitle = (TextView) alertDialog1
						.findViewById(R.id.firstLoginCEPUErrorTitleTVID);
				
				alertTitle.setTypeface(Utility.font_bold);
				textTV = (TextView) alertDialog1
						.findViewById(R.id.inValidFieldTVID);
				textTV.setTypeface(Utility.font_reg);
				Button okbutton = (Button) alertDialog1
						.findViewById(R.id.inValidOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				alertDialog1.setCancelable(false);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog1.dismiss();
					}
				});
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
	}

	boolean isEmailValid(CharSequence email) {
		   return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
		}
	
	@Override
	public boolean onLongClick(View v) {
		boolean returnValue = false;
		try {
			EditText ed = (EditText) v;
			int stringLength = ed.getText().length();
			returnValue = Utility.copyPasteMethod2(v, stringLength);
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
		return returnValue;
	}

}
