package com.myrewards.kununited.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.kununited.service.BayfordService;
import com.myrewards.kununited.service.BayfordServiceListener;
import com.myrewards.kununited.utils.Utility;

@SuppressLint("SetJavaScriptEnabled")
public class MyDealershipActivity extends Activity implements OnClickListener,
		BayfordServiceListener {

	Button btn1, btn2, backButton, scanBarBtn;
	TextView titleTV;;
	TextView text;
	WebView webView;
	View loading;
	String temp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_dealershipnew);

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		titleTV = (TextView) findViewById(R.id.titleTVID);
		titleTV.setTypeface(Utility.font_bold);
		titleTV.setText(getResources().getString(R.string.my_dealer_title));

		scanBarBtn = (Button) findViewById(R.id.scanBtnID);
		scanBarBtn.setVisibility(View.GONE);

		backButton = (Button) findViewById(R.id.backBtnID);
		backButton.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backButton.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);

		webView = (WebView) findViewById(R.id.webview);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setAllowFileAccess(true);
		webView.getSettings().setLoadsImagesAutomatically(true);

		backButton.setOnClickListener(this);

		loading = (View) findViewById(R.id.loading);
		loading.setVisibility(View.GONE);
		if (Utility
				.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			loading.setVisibility(View.VISIBLE);
			BayfordService.getBayfordService().sendTestRequestDeals(this);
		} else {

			LayoutInflater inflater = getLayoutInflater();
			View layout = inflater.inflate(R.layout.toast_no_netowrk,
					(ViewGroup) findViewById(R.id.custom_toast_layout_id));

			// The actual toast generated here.
			Toast toast = new Toast(getApplicationContext());
			toast.setDuration(Toast.LENGTH_LONG);
			toast.setView(layout);
			toast.show();

			if (Environment.getExternalStorageState().equals(
					Environment.MEDIA_MOUNTED)) {
				try {
					File root = new File(
							Environment.getExternalStorageDirectory()
									+ "/BAYFORD");
					File file = new File(root, "myDealership.html");
					if (file.exists()) {
						// Do action
						webView.loadUrl("file://"
								+ Environment.getExternalStorageDirectory()
								+ "/BAYFORD" + "/myDealership.html");
					}
				} catch (Exception e) {
					if (e != null) {
						Log.w("Hari-->DEBUG", e);
						e.printStackTrace();
					}
				}
			}
		}
	}

	@SuppressWarnings("unused")
	@Override
	public void onClick(View v) {
		Intent browserIntent;
		String url;
		switch (v.getId()) {
		case R.id.backBtnID:
			finish();
			break;
		}
	}

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public void onServiceComplete(Object response, int eventType) {
		loading.setVisibility(View.GONE);
		if (response != null) {
			if (eventType != 16) {
				try {
					String responseSrting = response.toString();
					temp = responseSrting.split("<root>")[0];
					responseSrting = responseSrting.split("<root>")[1];
					responseSrting = responseSrting.replaceFirst("</root>",
							"</body>");
					String summary = "<html>" + "<body>" + responseSrting
							+ "</html>";
					summary = Html.fromHtml(summary).toString();
					// create text file
					if (!Environment.getExternalStorageState().equals(
							Environment.MEDIA_MOUNTED))
						Log.d("BAYFORD", "No SDCARD");
					else {
						File direct = new File(
								Environment.getExternalStorageDirectory()
										+ "/BAYFORD");

						if (!direct.exists()) {
							if (direct.mkdir()) {
								// directory is created;
							}

						}

						try {
							File root = new File(
									Environment.getExternalStorageDirectory()
											+ "/BAYFORD");
							if (root.canWrite()) {
								File file = new File(root, "myDealership.html");
								FileWriter fileWriter = new FileWriter(file);
								BufferedWriter out = new BufferedWriter(
										fileWriter);
								out.write(summary);
								out.close();
							}
						} catch (IOException e) {
							Log.e("BAYFORD",
									"Could not write file " + e.getMessage());
						}
					}

					if (!Environment.getExternalStorageState().equals(
							Environment.MEDIA_MOUNTED)) {
						Log.d("BAYFORD", "No SDCARD");
					} else {

						webView.loadUrl("file://"
								+ Environment.getExternalStorageDirectory()
								+ "/BAYFORD" + "/myDealership.html");
					}
				} catch (Exception e) {

				}
			}
		}
	}
}
