package com.myrewards.kununited.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.myrewards.kununited.utils.ApplicationConstants;
import com.myrewards.kununited.utils.Utility;

public class GINNoticeBoardDetailsActivity extends BaseActivity implements
		OnClickListener {
	View loading;
	String noticeId;
	String noticeSubject = null;
	String noticeDetails = null;
	Button backBtn;
	TextView subjectTV;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notice_board_details_gin);
		setHeaderTitle(getResources().getString(R.string.my_notice_board));
		RelativeLayout v = (RelativeLayout) findViewById(R.id.headerRLID);
		v.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);
		v.setVisibility(View.VISIBLE);
		showBackButton();
		menuBtn = (Button) findViewById(R.id.menuBtnID);
		backBtn = (Button) findViewById(R.id.backToSearchBtnID);
		backBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		backBtn.setOnClickListener(this);
		menuListView = (ListView) findViewById(R.id.menuListViewID);
		initialiseViews();
		loading = (View) findViewById(R.id.loading);
		loading.setVisibility(View.GONE);

		if (getIntent() != null) {
			try {
				int colorResource = 0;
				Bundle bundle = getIntent().getExtras();
				if (bundle != null) {
					noticeId = bundle
							.getString(ApplicationConstants.NOTICE_ID_KEY_GIN);
					noticeSubject = bundle
							.getString(ApplicationConstants.NOTICE_NAME_KEY_GIN);
					noticeDetails = bundle
							.getString(ApplicationConstants.NOTICE_DETAILS_KEY_GIN);
					colorResource = bundle
							.getInt(ApplicationConstants.COLOR_CODE_KEY);
				}
				View resultListItem = (View) findViewById(R.id.resultListItemID);
				switch (colorResource) {
				case 0:
					resultListItem
							.setBackgroundResource(R.color.result_color_one);
					break;
				case 1:
					resultListItem
							.setBackgroundResource(R.color.result_color_two);
					break;
				case 2:
					resultListItem
							.setBackgroundResource(R.color.result_color_three);
					break;
				case 3:
					resultListItem
							.setBackgroundResource(R.color.result_color_four);
					break;
				}
				LinearLayout rowLL2 = (LinearLayout) findViewById(R.id.resultListItemHariLLID);
				rowLL2.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL | Gravity.CENTER);
				rowLL2.getLayoutParams().height = (int) (Utility.screenHeight / 9.4);
				TextView offNameTV = (TextView)findViewById(R.id.offerTVID);
				offNameTV.setVisibility(View.GONE);
				TextView noticeNameTV = (TextView)findViewById(R.id.productTVID);
				noticeNameTV.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL | Gravity.CENTER);
				noticeNameTV.setTypeface(Utility.font_bold);
				noticeNameTV.setText(""+noticeSubject);
				if (colorResource % 2 == 0) {
					noticeNameTV.setTextColor(Color.parseColor("#FFFFFF"));
				} else if (colorResource % 2 == 1) {
					noticeNameTV.setTextColor(Color.parseColor("#427CDC"));
				}
				/*	TextView noticeDetailsTV = (TextView)findViewById(R.id.noticeBoardDetailsTVID);
				noticeDetailsTV.setTypeface(Utility.font_reg);
				noticeDetailsTV.setText(""+Html.fromHtml(noticeDetails));*/
				
				loadInWebView(noticeDetails);
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.backToSearchBtnID:
			finish();
			break;
		}
	}
	
	private void loadInWebView(String webContent) {
		WebView webView = (WebView) findViewById(R.id.detailsTVID);
	
		webView.getSettings()
		.setJavaScriptEnabled(true);
webView.getSettings().setAllowFileAccess(true);
webView.getSettings()
		.setLoadsImagesAutomatically(true);
webView.getSettings().setUserAgentString(Locale.getDefault().getLanguage());
WebSettings settings = webView.getSettings();
settings.setDefaultTextEncodingName("utf-8");
webView.setWebViewClient(new WebViewClient());
webView.setWebViewClient(new WebViewClient() {

	@Override
	public boolean shouldOverrideUrlLoading(
			WebView view, String url) {
		if (url.startsWith("tel:")) {
			Intent intent = new Intent(
					Intent.ACTION_DIAL, Uri.parse(url));
			startActivity(intent);
		} else if (url.startsWith("http:")
				|| url.startsWith("https:")) {
			Intent intent = new Intent(
					Intent.ACTION_VIEW, Uri.parse(url));
			startActivity(intent);
		} else if (url.startsWith("mailto:")) {
			MailTo mt = MailTo.parse(url);
			Intent i = EmailIntent(GINNoticeBoardDetailsActivity.this,mt.getTo(), mt.getSubject(),
					mt.getBody(), mt.getCc());
			startActivity(i);
			view.reload();
			return true;
		} else {
			view.loadUrl(url);
		}
		return true;
	}
});
/*
 * String summary = Html.fromHtml(
 * product.getDetails() + "\n" + "\n" +
 * product.getText()).toString();
 */
String summary = "<html><body style=\"font-family:Helvetica;line-height:20px\">"
		+ webContent
		+ "</body></html>";

summary = summary.replaceAll("//", "");
// create text file
if (!Environment.getExternalStorageState()
		.equals(Environment.MEDIA_MOUNTED))
	Log.d("GINKUN", "No SDCARD");
else {
	File direct = new File(
			Environment
					.getExternalStorageDirectory()
					+ "/GINKUN");

	if (!direct.exists()) {
		if (direct.mkdir()) {
			// directory is created;
		}
	}

	try {
		File root = new File(
				Environment
						.getExternalStorageDirectory()
						+ "/GINKUN");
		if (root.canWrite()) {
			File file = new File(root,
					"GINKUNnoticedetails.html");
			FileWriter fileWriter = new FileWriter(
					file);
			BufferedWriter out = new BufferedWriter(
					fileWriter);
			if (summary.contains("<iframe")) {
				try {
					int a = summary
							.indexOf("<iframe");
					int b = summary
							.indexOf("</iframe>");
					summary = summary
							.replace(
									summary.subSequence(
											a,
											b),
									"");
				} catch (Exception e) {
					if (e != null) {
						e.printStackTrace();
						
					}
				}
			}
			out.write(summary);
			out.close();
		}
	} catch (IOException e) {
		if (e != null) {
			e.printStackTrace();
		
		}
	}
	
	if (!Environment.getExternalStorageState()
			.equals(Environment.MEDIA_MOUNTED)) {
		Log.d("GINKUN", "No SDCARD");
	} else {

		webView.loadUrl("file://"
				+ Environment
						.getExternalStorageDirectory()
				+ "/GINKUN"
				+ "/GINKUNnoticedetails.html");
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onReceivedError(
					WebView view, int errorCode,
					String description,
					String failingUrl) {
				Log.i("WEB_VIEW_TEST",
						"error code:" + errorCode);

				super.onReceivedError(view,
						errorCode, description,
						failingUrl);
			}
		});

		
	}
}
}
	
	
	
	public static Intent EmailIntent(Context context, String address,
			String subject, String body, String cc) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.putExtra(Intent.EXTRA_EMAIL, new String[] { address });
		intent.putExtra(Intent.EXTRA_TEXT, body);
		intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		intent.putExtra(Intent.EXTRA_CC, cc);
		intent.setType("message/rfc822");
		return intent;
	}
	
}
