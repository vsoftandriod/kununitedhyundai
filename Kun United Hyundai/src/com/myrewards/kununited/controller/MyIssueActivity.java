package com.myrewards.kununited.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.kununited.utils.Utility;

public class MyIssueActivity extends Activity implements OnClickListener,
		OnLongClickListener {
	EditText nameET, membershipET, messageET;
	Button sendbtn, deletebtn, backButton, scanBarBtn;
	TextView titleTV;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_issue);

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		titleTV = (TextView) findViewById(R.id.titleTVID);
		titleTV.setText(getResources().getString(R.string.my_issue));

		scanBarBtn = (Button) findViewById(R.id.scanBtnID);
		scanBarBtn.setVisibility(View.GONE);

		nameET = (EditText) findViewById(R.id.nameETID);
		backButton = (Button) findViewById(R.id.backBtnID);
		backButton.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backButton.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		backButton.setOnClickListener(this);
		membershipET = (EditText) findViewById(R.id.membershipETID);
		messageET = (EditText) findViewById(R.id.messageETID);
		sendbtn = (Button) findViewById(R.id.sendbtnID);
		sendbtn.getLayoutParams().width = (int) (Utility.screenWidth / 4.8);
		sendbtn.getLayoutParams().height = (int) (Utility.screenHeight / 18.7);
		sendbtn.setOnClickListener(this);
		deletebtn = (Button) findViewById(R.id.deletebtnID);
		deletebtn.getLayoutParams().width = (int) (Utility.screenWidth / 4.8);
		deletebtn.getLayoutParams().height = (int) (Utility.screenHeight / 18.7);
		deletebtn.setOnClickListener(this);
		nameET.setOnLongClickListener(this);
		membershipET.setOnLongClickListener(this);
		messageET.setOnLongClickListener(this);

		setEditBoxesText();
	}

	private void setEditBoxesText() {
		nameET.setText(Utility.user.getFirst_name() + " "
				+ Utility.user.getLast_name());
		membershipET.setText(Utility.user.getUsername());
	}

	@Override
	public void onClick(View arg0) {

		if (arg0.getId() == R.id.sendbtnID) {

			if (Utility
					.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				String toClientemail = getResources().getString(
						R.string.send_email_hint);
				// String
				// toSreenEmail=getResources().getString(R.string.sreen_email_hint);
				String message = messageET.getText().toString();

				Intent email = new Intent(Intent.ACTION_SEND);
				email.putExtra(Intent.EXTRA_EMAIL,
						new String[] { toClientemail });
				email.putExtra(Intent.EXTRA_SUBJECT,
						getResources().getString(R.string.my_issue));
				email.putExtra(Intent.EXTRA_TEXT, message);

				// need this to prompts email client only
				email.setType("message/rfc822");

				startActivity(Intent.createChooser(email,
						"Choose an Email client :"));
			} else {
				// The Custom Toast Layout Imported here
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_no_netowrk,
						(ViewGroup) findViewById(R.id.custom_toast_layout_id));

				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
			}
		}

		if (arg0.getId() == R.id.deletebtnID) {
			messageET.setText("");
		}
		if (arg0.getId() == R.id.backBtnID) {
			finish();
		}
	}

	@Override
	public boolean onLongClick(View v) {
		boolean returnValue = false;
		try {
			EditText ed = (EditText) v;
			int stringLength = ed.getText().length();
			returnValue = Utility.copyPasteMethod(v, stringLength);
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
		return returnValue;
	}
}
