package com.myrewards.kununited.controller;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.kununited.model.Product;
import com.myrewards.kununited.model.ProductDetails;
import com.myrewards.kununited.utils.ApplicationConstants;
import com.myrewards.kununited.utils.DatabaseHelper;
import com.myrewards.kununited.utils.Utility;

public class FavoritesListActivity extends BaseActivity {
	List<Product> myFavoriteProductsList;
	// List<Product> hotOffersProductsList;
	ProductDetails product;
	MyFavoritesAdapter mAdapter;
	LayoutInflater inflater;
	View loading;
	ListView favoritesListView;
	TextView noFavouritesadded;
	DatabaseHelper dbHelper;
	String productName = null;
	Button backButton;
	RelativeLayout headerRL;
	private final int DELETE_MY_FAVORITE_BUTTON = 1;
	public static int position;
	TextView tv22;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.results_list);

		setUpFavouritesScreen();

	}

	@Override
	protected void onResume() {
		super.onResume();
		setUpFavouritesScreen();
	}

	private void setUpFavouritesScreen() {
		try {
			setHeaderTitle(getResources()
					.getString(R.string.my_favourites_text));
			headerRL = (RelativeLayout) findViewById(R.id.headerRLID);
			headerRL.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);
			headerRL.setVisibility(View.GONE);

			loading = (View) findViewById(R.id.loading);
			loading.setVisibility(View.GONE);

			View header = (View) findViewById(R.id.headerID);
			header.setVisibility(View.GONE);

			dbHelper = new DatabaseHelper(this);
			menuBtn = (Button) findViewById(R.id.menuBtnID);
			menuListView = (ListView) findViewById(R.id.menuListViewID);
			initialiseViews();
			favoritesListView = (ListView) findViewById(R.id.resultsListViewID);
			myFavoriteProductsList = new ArrayList<Product>();
			favoritesListView.setOnItemClickListener(this);
			try {
				myFavoriteProductsList = dbHelper.getProductList();
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();

				}
			}

			noFavouritesadded = (TextView) findViewById(R.id.noFavAdded_deleteTVID);
			noFavouritesadded.setTypeface(Utility.font_bold);
			noFavouritesadded.setEllipsize(TextUtils.TruncateAt.MARQUEE);
			if (myFavoriteProductsList.size() != 0) {
				mAdapter = new MyFavoritesAdapter(FavoritesListActivity.this);
				favoritesListView.setAdapter(mAdapter);
				mAdapter.notifyDataSetChanged();
				noFavouritesadded.setVisibility(View.GONE);
			} else {
				noFavouritesadded.setVisibility(View.VISIBLE);
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
			}
		}
	}

	public class MyFavoritesAdapter extends BaseAdapter {
		Context ctx;

		public MyFavoritesAdapter(FavoritesListActivity favoritesListActivity) {
			this.ctx = favoritesListActivity;

		}

		@Override
		public int getCount() {
			return myFavoriteProductsList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(final int pos, View view, ViewGroup arg2) {
			View resultsListRow = null;
			try {
				if (resultsListRow == null) {
					inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					resultsListRow = (View) inflater.inflate(
							R.layout.my_favorites_list_item, null, false);
				}
				RelativeLayout rowLL = (RelativeLayout) resultsListRow
						.findViewById(R.id.favResultListItemLLID);
				RelativeLayout rowLL2 = (RelativeLayout) resultsListRow
						.findViewById(R.id.resultItemLLID);
				rowLL2.getLayoutParams().height = (int) (Utility.screenHeight / 8.2);
				TextView productNameTV = (TextView) resultsListRow
						.findViewById(R.id.productMyTVID);
				productNameTV.setTypeface(Utility.font_bold);
				TextView highlightTV = (TextView) resultsListRow
						.findViewById(R.id.offerMyTVID);
				highlightTV.setTypeface(Utility.font_reg);
				Button deleteBtn = (Button) resultsListRow
						.findViewById(R.id.deleteMyFavoriteBtnID);
				deleteBtn.setFocusable(false);
				deleteBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {

						if (v.getId() == R.id.deleteMyFavoriteBtnID) {

							// ------------ Harikrishna---------- //
							Utility.fav_position_var = pos;
							showDialog(DELETE_MY_FAVORITE_BUTTON);
						} else {
							if (menuListView.getVisibility() == ListView.GONE) {
								Intent detailsIntent = new Intent(
										FavoritesListActivity.this,
										ProductDetailsActivity.class);
								detailsIntent
										.putExtra(
												ApplicationConstants.PRODUCT_ID_KEY,
												myFavoriteProductsList.get(pos)
														.getId());
								detailsIntent.putExtra(
										ApplicationConstants.COLOR_CODE_KEY,
										pos % 2);
								detailsIntent.putExtra(
										ApplicationConstants.PRODUCT_NAME_KEY,
										myFavoriteProductsList.get(pos)
												.getName());
								detailsIntent
										.putExtra(
												ApplicationConstants.PRODUCT_HIGHLIGHT_KEY,
												myFavoriteProductsList.get(pos)
														.getHighlight());
								startActivity(detailsIntent);
								mAdapter.notifyDataSetChanged();
							}
						}
					}
				});

				productNameTV
						.setText(myFavoriteProductsList.get(pos).getName());
				highlightTV.setText(myFavoriteProductsList.get(pos)
						.getHighlight());
				switch (pos % 2) {
				case 0:
					rowLL.setBackgroundResource(R.color.result_color_one);
					productNameTV.setTextColor(Color.parseColor("#FFFFFF"));
					break;
				case 1:
					rowLL.setBackgroundResource(R.color.result_color_two);
					productNameTV.setTextColor(Color.parseColor("#427CDC"));
					break;
				}
				Animation animation = AnimationUtils.loadAnimation(ctx,
						R.anim.push_left_in);
				resultsListRow.startAnimation(animation);
				animation = null;
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
					e.printStackTrace();
				}
				return null;
			}

			return resultsListRow;
		}
	}

	// Hari----------------------
	protected Dialog onCreateDialog(int id) {
		try {
			if (id == 1) {
				AlertDialog deleteFavAlert = null;
				switch (id) {
				case DELETE_MY_FAVORITE_BUTTON:

					LayoutInflater liDelete = LayoutInflater.from(this);
					View deleteFavView = liDelete.inflate(
							R.layout.dialog_layout_delete_favorite, null);
					AlertDialog.Builder adbDeleteFav = new AlertDialog.Builder(
							this);
					adbDeleteFav.setCancelable(false);
					adbDeleteFav.setView(deleteFavView);
					deleteFavAlert = adbDeleteFav.create();
					break;
				}
				return deleteFavAlert;

			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
		return super.onCreateDialog(id);
	}

	protected void onPrepareDialog(int id, Dialog dialog) {
		try {
			if (id == 1) {
				switch (id) {
				case DELETE_MY_FAVORITE_BUTTON:
					final AlertDialog alt3 = (AlertDialog) dialog;

					TextView alertTitle = (TextView) alt3
							.findViewById(R.id.firstLoginCEPUTitleTVID);

					alertTitle.setTypeface(Utility.font_bold);

					tv22 = (TextView) alt3.findViewById(R.id.deleteFavTVID);
					tv22.setTypeface(Utility.font_reg);
					tv22.setText("Are you sure you want to delete"
							+ " "
							+ myFavoriteProductsList.get(
									Utility.fav_position_var).getName() + " "
							+ "from favourites?");
					Button deleteFavYesBtn = (Button) alt3
							.findViewById(R.id.delete_fav_yesBtnID);
					deleteFavYesBtn.setTypeface(Utility.font_bold);
					alt3.setCancelable(false);
					Button deleteNoFavBtn = (Button) alt3
							.findViewById(R.id.delete_fav_noBtnID);
					deleteNoFavBtn.setTypeface(Utility.font_bold);
					deleteFavYesBtn
							.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									dbHelper.deleteProduct(Integer
											.toString(myFavoriteProductsList
													.get(Utility.fav_position_var)
													.getId()));
									myFavoriteProductsList = dbHelper
											.getProductList();
									mAdapter.notifyDataSetChanged();
									if (myFavoriteProductsList.size() == 0) {
										noFavouritesadded = (TextView) findViewById(R.id.noFavAdded_deleteTVID);
										noFavouritesadded
												.setVisibility(View.VISIBLE);
									}
									alt3.dismiss();
								}
							});
					deleteNoFavBtn.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							alt3.dismiss();
						}
					});
					break;
				}
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View rowView, int pos,
			long arg3) {
		if (Utility
				.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			try {
				super.onItemClick(arg0, rowView, pos, arg3);
				if (menuListView.getVisibility() == ListView.GONE) {
					Intent detailsIntent = new Intent(
							FavoritesListActivity.this,
							ProductDetailsActivity.class);
					detailsIntent.putExtra(ApplicationConstants.PRODUCT_ID_KEY,
							myFavoriteProductsList.get(pos).getId());
					detailsIntent.putExtra(ApplicationConstants.COLOR_CODE_KEY,
							pos % 2);
					detailsIntent.putExtra(
							ApplicationConstants.PRODUCT_NAME_KEY,
							myFavoriteProductsList.get(pos).getName());
					detailsIntent.putExtra(
							ApplicationConstants.PRODUCT_HIGHLIGHT_KEY,
							myFavoriteProductsList.get(pos).getHighlight());
					startActivity(detailsIntent);
					mAdapter.notifyDataSetChanged();
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
					e.printStackTrace();
				}
			}
		} else {
			// The Custom Toast Layout Imported here
			LayoutInflater inflater = getLayoutInflater();
			View layout = inflater.inflate(R.layout.toast_no_netowrk,
					(ViewGroup) findViewById(R.id.custom_toast_layout_id));

			// The actual toast generated here.
			Toast toast = new Toast(getApplicationContext());
			toast.setDuration(Toast.LENGTH_LONG);
			toast.setView(layout);
			toast.show();
		}
	}
}
