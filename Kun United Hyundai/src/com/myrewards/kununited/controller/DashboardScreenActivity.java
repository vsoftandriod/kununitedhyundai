package com.myrewards.kununited.controller;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.kununited.model.NoticeId;
import com.myrewards.kununited.utils.DatabaseHelper;
import com.myrewards.kununited.utils.Utility;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseInstallation;
import com.parse.PushService;
import com.readystatesoftware.viewbadger.BadgeView;

/**
 * 
 * @author HARI This class is used to display like menu
 */

public class DashboardScreenActivity extends Activity implements
		OnClickListener {

	// custom dialog fields
	public TextView logoutText;
	public LinearLayout logoutSizeLL;
	public Button loginbutton1;
	public Button cancelbutton1;
	final private static int LOGOUT = 1;
	public ImageView lalertImg;
	public TextView textLog;
	ImageView noticeboardBadge;
	DatabaseHelper helper;
	static int noticeCount = 0;
	public ImageView noticeboardunread;
	public static boolean noticeboardcount = false;
	ArrayList<NoticeId> noticeid;
	DatabaseHelper dbHelper;

	/** Called when the activity is first created. */

	boolean doubleBackToExitPressedOnce = false;
	WakeLock wakeLock;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard1);

		
		getWindow().setSoftInputMode(
			    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
			);
		
		/*getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
				"MyWakelockTag");*/

		dbHelper = new DatabaseHelper(this);

		// Push Notifications Adding...................
		// Parse.initialize(this, YOUR_APPLICATION_ID, YOUR_CLIENT_KEY);
		try {
			Parse.initialize(this, "33hR7DkQ6BLOc1tokzn8KZzELTSdOuOnpfsNahJj",
					"rtRTzrJLy2sjuvPVHfylyGPc0GeNX2kowVKpLNjD");

			PushService.setDefaultPushCallback(this,
					AppPushNotificationActivity.class);
			ParseInstallation.getCurrentInstallation().saveInBackground();
			ParseAnalytics.trackAppOpened(getIntent());
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->DEBUG", e);
			}
		}

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerIVID);
		headerImage.getLayoutParams().height = Utility.screenHeight / 10;

		RelativeLayout myUnionRL = (RelativeLayout) findViewById(R.id.myUnionRLID);
		myUnionRL.setOnClickListener(this);
		RelativeLayout myDelegatesRL = (RelativeLayout) findViewById(R.id.myDelegatesRLID);
		myDelegatesRL.setOnClickListener(this);
		RelativeLayout myCardRL = (RelativeLayout) findViewById(R.id.myCardRLID);
		myCardRL.setOnClickListener(this);
		RelativeLayout myUnionContactsRL = (RelativeLayout) findViewById(R.id.myUnionContactsRLID);
		myUnionContactsRL.setOnClickListener(this);
		RelativeLayout myNoticeBoardRL = (RelativeLayout) findViewById(R.id.myNoticeBoardRLID);
		myNoticeBoardRL.setOnClickListener(this);
		RelativeLayout sendAFriendRL = (RelativeLayout) findViewById(R.id.sendAFriendRLID);
		sendAFriendRL.setOnClickListener(this);
		helper = new DatabaseHelper(DashboardScreenActivity.this);
		noticeboardunread = (ImageView) findViewById(R.id.noticeBadgeID);
		BadgeView badge = new BadgeView(DashboardScreenActivity.this,
				noticeboardunread);

		try {
			if (helper.getNoticeDetails() != 0) {
				badge.setText(Integer.toString(helper.getNoticeDetails()));
				badge.setBadgeBackgroundColor(Color.parseColor("#007ED4"));
				// badge.setBackgroundColor(Color.RED);
				badge.setBadgePosition(BadgeView.POSITION_BOTTOM_LEFT);
				badge.show();
			} else {
				badge.setVisibility(View.GONE);
				noticeboardunread.setVisibility(View.GONE);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
		RelativeLayout mySpecialOffersRL = (RelativeLayout) findViewById(R.id.mySpecialOffersRLID);
		mySpecialOffersRL.setOnClickListener(this);
		RelativeLayout searchMyRewardsRL = (RelativeLayout) findViewById(R.id.searchMyRewardsRLID);
		searchMyRewardsRL.setOnClickListener(this);
		// set logout from cepu

		RelativeLayout lagoutRL = (RelativeLayout) findViewById(R.id.lagoutRLID);
		lagoutRL.setOnClickListener(this);
		/*
		 * lagoutRL.setOnClickListener(new View.OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { showDialog(LOGOUT); } });
		 */

		RelativeLayout myParkingTimeRL = (RelativeLayout) findViewById(R.id.myParkingTimeRLID);
		myParkingTimeRL.setOnClickListener(this);
		RelativeLayout myissueRL = (RelativeLayout) findViewById(R.id.myissueRLID);
		myissueRL.setOnClickListener(this);
		RelativeLayout myAccountRL = (RelativeLayout) findViewById(R.id.myAccountRLID);
		myAccountRL.setOnClickListener(this);
	}

	// to create custom alerts
	protected Dialog onCreateDialog(int id) {
		AlertDialog dialogDetails = null;
		LayoutInflater inflater;
		AlertDialog.Builder dialogbuilder;
		View dialogview;
		switch (id) {
		case LOGOUT:
			inflater = LayoutInflater.from(this);
			dialogview = inflater.inflate(R.layout.dialog_layout_logout, null);
			dialogbuilder = new AlertDialog.Builder(this);
			dialogbuilder.setCancelable(false);
			dialogbuilder.setView(dialogview);
			dialogDetails = dialogbuilder.create();
			break;

		case 2:
			inflater = LayoutInflater.from(this);
			dialogview = inflater.inflate(R.layout.dialog_layout_logout, null);
			dialogbuilder = new AlertDialog.Builder(this);
			dialogbuilder.setCancelable(false);
			dialogbuilder.setView(dialogview);
			dialogDetails = dialogbuilder.create();

			break;
		}
		return dialogDetails;
	}

	protected void onPrepareDialog(int id, Dialog dialog) {
		try {
			switch (id) {
			case LOGOUT:
				final AlertDialog alertDialog1 = (AlertDialog) dialog;
				TextView alertTitle = (TextView) alertDialog1
						.findViewById(R.id.alertLogoutTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);
				logoutText = (TextView) alertDialog1
						.findViewById(R.id.my_logoutTVID);
				logoutText.setTypeface(Utility.font_reg);
				loginbutton1 = (Button) alertDialog1
						.findViewById(R.id.loginBtnH_ID);
				loginbutton1.setTypeface(Utility.font_bold);
				cancelbutton1 = (Button) alertDialog1
						.findViewById(R.id.btn_cancelH_ID);
				cancelbutton1.setTypeface(Utility.font_bold);
				alertDialog1.setCancelable(false);

				loginbutton1.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						try {
							helper.deleteLoginDetails();

						} catch (Exception e) {
							Log.w("Hari-->", e);
						}
						Intent logoutIntent = new Intent(
								DashboardScreenActivity.this,
								LoginScreenActivity.class);
						logoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_SINGLE_TOP);
						startActivity(logoutIntent);
						LoginScreenActivity.etUserId.setText("");
						LoginScreenActivity.etPasswd.setText("");
						DashboardScreenActivity.this.finish();
						alertDialog1.dismiss();
					}
				});
				cancelbutton1.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog1.dismiss();
					}
				});
				break;

			case 2:
				final AlertDialog alertDialog2 = (AlertDialog) dialog;
				TextView alertTitleTV = (TextView) alertDialog2
						.findViewById(R.id.alertLogoutTitleTVID);
				alertTitleTV.setTypeface(Utility.font_bold);
				alertTitleTV.setText("Exit !");
				logoutText = (TextView) alertDialog2
						.findViewById(R.id.my_logoutTVID);
				logoutText.setTypeface(Utility.font_reg);
				logoutText.setText("Do you want to exit the Application ?");
				loginbutton1 = (Button) alertDialog2
						.findViewById(R.id.loginBtnH_ID);
				loginbutton1.setText("YES");
				loginbutton1.setTypeface(Utility.font_bold);
				cancelbutton1 = (Button) alertDialog2
						.findViewById(R.id.btn_cancelH_ID);
				cancelbutton1.setText("NO");
				cancelbutton1.setTypeface(Utility.font_bold);
				alertDialog2.setCancelable(false);
				loginbutton1.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {

						finish();
					}
				});
				cancelbutton1.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog2.dismiss();
					}
				});
				break;
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.myUnionRLID:
			Intent intentMyUnion = new Intent(DashboardScreenActivity.this,
					MyDealershipActivity.class);
			intentMyUnion.putExtra(Utility.DASHBOARD_ICON_ID,
					Utility.MY_DEALERSHIP);
			startActivity(intentMyUnion);
			break;
		case R.id.myDelegatesRLID:
			Intent intentMyDelegates = new Intent(DashboardScreenActivity.this,
					MyCarActivity.class);
			intentMyDelegates.putExtra(Utility.DASHBOARD_ICON_ID,
					Utility.MY_CAR);
			startActivity(intentMyDelegates);
			break;
		case R.id.myCardRLID:
			Intent intentMyCard = new Intent(DashboardScreenActivity.this,
					MyRoadsideAssist.class);
			intentMyCard.putExtra(Utility.DASHBOARD_ICON_ID,
					Utility.MY_ROADSIDE);
			startActivity(intentMyCard);
			break;
		case R.id.myUnionContactsRLID:
			Intent intentMyUnionContacts = new Intent(
					DashboardScreenActivity.this, BookAServiceActivity.class);
			intentMyUnionContacts.putExtra(Utility.DASHBOARD_ICON_ID,
					Utility.BOOK_A_SERVICE);
			startActivity(intentMyUnionContacts);
			break;
		case R.id.myNoticeBoardRLID:
			Intent intentMyNoticeBoard = new Intent(
					DashboardScreenActivity.this, NewParkingTimeActivity.class);
			intentMyNoticeBoard.putExtra(Utility.DASHBOARD_ICON_ID,
					Utility.MY_PARKING_TIME);
			startActivity(intentMyNoticeBoard);
			break;
		case R.id.sendAFriendRLID:
			if (Utility
					.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				Intent intentSendaFriend = new Intent(
						DashboardScreenActivity.this,
						MyNoticeBoardActivity.class);
				intentSendaFriend.putExtra(Utility.DASHBOARD_ICON_ID,
						Utility.MY_NOTICE_BOARD);
				startActivity(intentSendaFriend);
				DashboardScreenActivity.this.finish();
			} else {
				// The Custom Toast Layout Imported here
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_no_netowrk,
						(ViewGroup) findViewById(R.id.custom_toast_layout_id));

				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
			}
			break;
		case R.id.mySpecialOffersRLID:
			Intent intentGIN1 = new Intent(DashboardScreenActivity.this,
					SendAFriendNewActivity.class);
			intentGIN1.putExtra(Utility.DASHBOARD_ICON_ID,
					Utility.SEND_A_FRIEND);
			startActivity(intentGIN1);
			break;
		case R.id.searchMyRewardsRLID:
			if (Utility
					.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				Intent intentGIN2 = new Intent(DashboardScreenActivity.this,
						MyCardActivity.class);
				intentGIN2.putExtra(Utility.DASHBOARD_ICON_ID, Utility.MY_CARD);
				startActivity(intentGIN2);
			} else {
				// The Custom Toast Layout Imported here
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_no_netowrk,
						(ViewGroup) findViewById(R.id.custom_toast_layout_id));

				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
			}
			break;
		case R.id.myParkingTimeRLID:
			if (Utility
					.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				Intent intentMyParking = new Intent(
						DashboardScreenActivity.this, MyAccountActivity.class);
				intentMyParking.putExtra(Utility.DASHBOARD_ICON_ID,
						Utility.MY_ACCOUNT);
				startActivity(intentMyParking);
				DashboardScreenActivity.this.finish();
			} else {
				// The Custom Toast Layout Imported here
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_no_netowrk,
						(ViewGroup) findViewById(R.id.custom_toast_layout_id));

				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
			}
			break;
		case R.id.myissueRLID:
			if (Utility
					.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				Intent intentMyIssue = new Intent(DashboardScreenActivity.this,
						GrabitNowTabsActivity.class);
				intentMyIssue.putExtra(Utility.DASHBOARD_ICON_ID, 2);
				startActivity(intentMyIssue);
			} else {
				// The Custom Toast Layout Imported here
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_no_netowrk,
						(ViewGroup) findViewById(R.id.custom_toast_layout_id));

				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
			}
			break;
		case R.id.myAccountRLID:
			if (Utility
					.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				Intent intentMyAccount = new Intent(
						DashboardScreenActivity.this,
						GrabitNowTabsActivity.class);
				intentMyAccount.putExtra(Utility.DASHBOARD_ICON_ID, 1);
				startActivity(intentMyAccount);
			} else {
				// The Custom Toast Layout Imported here
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_no_netowrk,
						(ViewGroup) findViewById(R.id.custom_toast_layout_id));

				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
			}
			break;

		case R.id.lagoutRLID:
			if (Utility
					.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				Intent intentLikeMe = new Intent(DashboardScreenActivity.this,
						LikeActivity.class);

				startActivity(intentLikeMe);
			} else {
				// The Custom Toast Layout Imported here
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_no_netowrk,
						(ViewGroup) findViewById(R.id.custom_toast_layout_id));

				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
			}
			break;

		default:
			break;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			exitApplication();

			return true;
		} else if (keyCode == KeyEvent.KEYCODE_MENU) {

		}
		return super.onKeyDown(keyCode, event);
	}

	private void exitApplication() {

		if (doubleBackToExitPressedOnce) {

			DashboardScreenActivity.this.finish();

		} else {
			doubleBackToExitPressedOnce = true;
			Toast.makeText(DashboardScreenActivity.this,
					"Press again to minimize the app.", 100).show();
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					doubleBackToExitPressedOnce = false;
				}
			}, 2000);

		}

	}

	@Override
	protected void onDestroy() {
		
		super.onDestroy();
	}

	@Override
	protected void onStop() {

		super.onStop();
	}

	@Override
	protected void onResume() 
	{
		//if(wakeLock.isHeld())
		//wakeLock.release();
		super.onResume();
	}

	@Override
	protected void onPause() {
		/*if(!wakeLock.isHeld())
		wakeLock.acquire();*/
		super.onPause();
	}
}