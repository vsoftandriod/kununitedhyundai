package com.myrewards.kununited.controller;

import android.app.ListActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.myrewards.kununited.utils.OpenHelper;

public class MyCarDetailsActivity extends ListActivity {
	Cursor cr;
	ListView ls;
	OpenHelper openhelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		ls = getListView();
		openhelper = new OpenHelper(this);
		SQLiteDatabase db = openhelper.getWritableDatabase();
		cr = db.query("mycar", null, null, null, null, null, null);
		ls.setAdapter(new adapter());
	}

	class adapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return cr.getCount();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int pos, View cview, ViewGroup arg2) {
			View v;
			LayoutInflater inflater = getLayoutInflater();
			v = inflater.inflate(R.layout.cardetailsrow, null);
			TextView tv1 = (TextView) v.findViewById(R.id.textView1);
			TextView tv2 = (TextView) v.findViewById(R.id.textView2);
			TextView tv3 = (TextView) v.findViewById(R.id.textView3);
			TextView tv4 = (TextView) v.findViewById(R.id.textView4);
			TextView tv5 = (TextView) v.findViewById(R.id.textView5);
			cr.moveToFirst();
			cr.moveToPosition(pos);
			tv1.setText(cr.getString(cr.getColumnIndex("carMake")));
			tv2.setText(cr.getString(cr.getColumnIndex("carModel")));
			tv3.setText(cr.getString(cr.getColumnIndex("carRegistration")));
			tv4.setText(cr.getString(cr.getColumnIndex("carColor")));
			tv5.setText(cr.getString(cr.getColumnIndex("carYear")));
			return v;
		}

	}

}
