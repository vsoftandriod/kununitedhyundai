package com.myrewards.kununited.xml;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.myrewards.kununited.model.ProductAddress;

public class ProductAddressesParser {
	private List<ProductAddress> productAddressList;
	public void internalXMLParse(String response, List<ProductAddress> productAddress1) {
		try {
			this.productAddressList = productAddress1;
		String _node,_element;
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;
		
			db = factory.newDocumentBuilder();
		
        InputSource inStream = new InputSource();
        inStream.setCharacterStream(new StringReader(response));
        Document doc = db.parse(inStream);  
        doc.getDocumentElement().normalize();
        
        NodeList list2 =    doc.getElementsByTagName("merchant");
        _node = new String();
        _element = new String();      
        
        for (int i=0;i<list2.getLength();i++){    
        	NodeList childNodes = list2.item(i).getChildNodes();
        	 ProductAddress productAddress = new ProductAddress();
             for(int j=0;j<childNodes.getLength();j++){
            	 Node value=childNodes.item(j).getChildNodes().item(0);
            	 _node=childNodes.item(j).getNodeName();
                 if(value != null){
                	 _element=value.getNodeValue(); 
                	 updateField(_node,_element, productAddress);
                 }
             }
             productAddressList.add(productAddress);
        }
        } catch (ParserConfigurationException e) {
			e.printStackTrace();
        } catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private void updateField(String node, String element, ProductAddress productAddress) {
		if(node.equals("id"))
        {
			productAddress.setId(element);
        }
		else if(node.equals("address1"))
        {
			productAddress.setAddress1(element);
        }
		else if(node.equals("name"))
        {
			productAddress.setName(element);
        }
		
		else if(node.equals("city"))
        {
			productAddress.setCity(element);
        }
		else if(node.equals("state"))
        {
			productAddress.setState(element);
        }
		else if(node.equals("region"))
        {
			productAddress.setRegion(element);
        }
		else if(node.equals("country"))
        {
			productAddress.setCountry(element);
        }
		else if(node.equals("postcode"))
        {
			productAddress.setPostcode(element);
        }
		else if(node.equals("latitude"))
        {
			productAddress.setLatitude(element);
        }
		else if(node.equals("longitude"))
        {
			productAddress.setLongitude(element);
        }
		else if(node.equals("phone"))
        {
			productAddress.setPhone(element);
        }
		else if(node.equals("primary"))
        {
			productAddress.setPrimary(element);
        }
		else if(node.equals("suburb"))
		{
			productAddress.setSuburb(element);
		}
	}
}
