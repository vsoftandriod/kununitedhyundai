package com.myrewards.kununited.xml;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.util.Log;

import com.myrewards.kununited.model.NoticeId;
import com.myrewards.kununited.utils.DatabaseHelper;

public class NoticeboardIdParser {
	public static  int noticeboard_id_count=0;
	List<NoticeId> noticeidob;

	 DatabaseHelper helper;
	public void IdParser(String responsing,List<NoticeId> noticeidob) {
		 try {
			 this.noticeidob=noticeidob;
				String _node,_element;
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		        DocumentBuilder db;
				
					db = factory.newDocumentBuilder();
				
		        InputSource inStream = new InputSource();
		        inStream.setCharacterStream(new StringReader(responsing));
		        Document doc = db.parse(inStream);  
		        doc.getDocumentElement().normalize();
		        
		        NodeList list =    doc.getElementsByTagName("*");
		        _node = new String();
		        _element = new String();      
		        
		        for (int i=0;i<list.getLength();i++){ 
		        	NoticeId noticeob=new NoticeId();
		             Node value=list.item(i).		
		              getChildNodes().item(0);
		             _node=list.item(i).getNodeName();
		             if(value != null){
		            	 _element=value.getNodeValue(); 
		            	
						updateField(_node,_element,noticeob);
		             }
		             noticeidob.add(noticeob);
		        }
		     
		        } catch (ParserConfigurationException e) {
					e.printStackTrace();
		        } catch (SAXException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
	}
	public void updateField(String _node, String _element,NoticeId noticeid) {
		if(_node.equals("id"))
			
		{
			Log.v("hai ", "this is shambhi");
	if(_element!=null)
	{
			noticeid.setNoticeDetails(_element);
			
			 noticeboard_id_count++;
	}
	
			}

		   
		
	}
	

}
