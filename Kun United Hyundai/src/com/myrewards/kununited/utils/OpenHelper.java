package com.myrewards.kununited.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class OpenHelper extends SQLiteOpenHelper{
/*	public String MYPARKINGONE;
	public String MYPARKINGTWO;
	public String MYPARKINGTHREE;*/
	public OpenHelper(Context context)
	{
		super(context, "details",null,1);
	}
	@Override
	public void onCreate(SQLiteDatabase db) {
		
		db.execSQL("CREATE TABLE mycar(carMake TEXT,carModel TEXT,carRegistration TEXT,carColor TEXT,carYear TEXT)");
		db.execSQL("CREATE TABLE mytime(MYPARKINGONE TEXT,MYPARKINGTWO TEXT,MYPARKINGTHREE TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("UPDATE TABLE mycar add column carService TEXT");
		onCreate(db);
	}

}
