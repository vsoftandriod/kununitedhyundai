package com.myrewards.kununited.utils;

public class ApplicationConstants {

	public static final String subDomainURLString = "www.mycustomerprivileges.in";
	
	// SERVER MAIN ROOT URL
	public static String SERVER_ROOT = "http://www.myrewards.com.au/newapp/";

	// LOGIN URL'S
	public static final String LOGIN_WRAPPER = SERVER_ROOT + "login.php";
	public static final String USER_DETAILS_WRAPPER = SERVER_ROOT+ "get_user.php?uname=";

	// DIALY SVAERS FIRST TIME LOGIN DETAILS URL'S
	public static final String FIRSTTIME_LOGIN = SERVER_ROOT + "first_time_login.php?";
	public static final String FIRST_TIME_USER_URL = SERVER_ROOT + "first_time_user.php";
	
	// ABOUT US AND MY DEALS URL'S
	public static final String MY_DEAL_WRAPPER = "http://www.myrewards.com.au/newapp/get_page.php?cid=92&pid=1";

	// MY CARD URL..........
	public static final String MY_CARD_URL = "http://www.myrewards.com.au/files/mobile_membership_card/";

	// NOTICE BOARD DETAILS
	public static final String NOTICE_ID_KEY = "productIdKey";
	public static final String NOTICE_NAME_KEY = "productNameKey";
	public static final String NOTICE_DETAILS_KEY = "productHighlightKey";
	//NOTICE BOARD ACTUAL URL
	public static final String NOTICE_BOARD_WRAPPER_DETAILS = "http://myrewards.com.au/newapp/get_notice.php?cid=92";
	
	//  NOTICE BOARD ID URL
	public static String NOTICEBOARD_ID_URL = "http://myrewards.com.au/newapp/get_notice_ids.php?cid=92";

	// GRAB IT NOW STRING VALUES....................
	public static final String CATEGORY_LIST = "getCategoryList";
	public static final String CAT_ID_KEY = "catIdKey";
	public static final String LOCATION_KEY = "locationKey";
	public static final String KEYWORK_KEY = "keywordKey";
	public static final String PRODUCT_ID_KEY = "productIdKey";
	public static final String COLOR_CODE_KEY = "ColorCodeKey";
	public static final String PRODUCT_NAME_KEY = "productNameKey";
	public static final String PRODUCT_HIGHLIGHT_KEY = "productHighlightKey";
	public static final String PHP_SESSION_KEY = "PHPSESSID=";
	public static final String DAILYDEALSIMAGE = "IMAGE";

	// ALL PRODUCTS INFORMATION URL'S
	public static final String NEAREST_LAT_LON_WRAPPER = SERVER_ROOT+ "get_products_by_loc.php?";
	public static final String PRODUCT_LOGO_WRAPPER = "http://www.myrewards.com.au/files/merchant_logo/";
	
	public static final String PRODUCT_IMAGE_LOGO_WRAPPER = "http://www.myrewards.com.au/files/product_image/";
	
	public static final String PRODUCT_ADDRESSES_WRAPPER = SERVER_ROOT+ "get_product_addresses.php?pid=";
	public static final String HELP_SCREEN_WRAPPER = "http://www.myrewards.com.au/newapp/get_page.php?cid=67&pid=1";
	public static final String REDEEM_DETAILS_WRAPPER = SERVER_ROOT+ "redeemed.php";
	public static final String IMAGE_URL_KEY = "imageURL";
	
	// GRAB IT NOW HOT OFFERS URL
	public static final String HOT_OFFERS_WRAPPER = SERVER_ROOT	+ "get_hot_offer.php?cid=";

	// GRAB IT NOW SEARCH CATEGORIES URL'S
	public static final String CATEGORY_WRAPPER = SERVER_ROOT + "get_cat.php?";
	public static final String SEARCH_PRODUCTS_WRAPPER = SERVER_ROOT+ "search.php?";

	// GRAB IT NOW PRODUCTS DETAILS URL
	public static final String PRODUCT_DETAILS_WRAPPER = SERVER_ROOT+ "get_product.php?";

	// GRAB IT NOW CLIENT BANNER URL
	public static final String CLIENT_BANNER_WRAPPER = SERVER_ROOT+ "get_client_banner.php?cid=";

	// GRAB IT NOW NOTICE BOARD URL'S
	public static final String NOTICE_BOARD_WRAPPER = "http://myrewards.com.au/newapp/get_notice.php?cid=1";

	public static final String NOTICE_ID_KEY_GIN = "productIdKey";
	public static final String NOTICE_NAME_KEY_GIN = "productNameKey";
	public static final String NOTICE_DETAILS_KEY_GIN = "productHighlightKey";

	// GRAB IT NOW DAILY DEALS URL'S
	public static final String DAILY_DEALS_WRAPPER = "http://www.myrewards.com.au/newapp/get_daily_deal.php?";
	public static final String DAILY_DEALS_IMAGE_WRAPPER = "http://www.myrewards.com.au/files/hot_offer_image/";

	// MY ACCOUNT URL
	public static final String MY_ACCOUNT_WRAPPER = SERVER_ROOT + "update_user_info.php";
	// CONNECTION PROBLEMS
	public static String UNABLETOESTABLISHCONNECTION = "Unable to establish the connection.";
	public static String UNABLETOESTABLISHCONNECTION_URL = "Loading please wait...";
	
	// this is for send a friend
	public static final String SEND_A_FRIEND_ID_URL = SERVER_ROOT + "saf.php";
}
