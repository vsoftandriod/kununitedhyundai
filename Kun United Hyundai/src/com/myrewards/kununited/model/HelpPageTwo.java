package com.myrewards.kununited.model;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.myrewards.kununited.controller.R;
import com.myrewards.kununited.utils.Utility;

/**
 * @author HARI
 *
 */
public class HelpPageTwo extends Fragment {

	TextView tv;
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) 
	{
		View v=(LinearLayout)inflater.inflate(R.layout.page2_frag2_layout, container, false);
		TextView textview1=(TextView)v.findViewById(R.id.page1_IVID);
		textview1.setMovementMethod(new ScrollingMovementMethod());
		textview1.setTypeface(Utility.font_reg);
		textview1.setText(Html.fromHtml("<h4>  Searching  for discounts is EASY! </h4> &#9733; Go to <b>Search</b> by category  in the menu... <br><br>&#9733;  Select an <b>offer category</b> that interests you... <br><br>&#9733;  Include the <b>suburb or city</b> you  are searching... <br><br>&#9733;  Add a <b>keyword</b> to help refine your search... <br><br><br><br><br><br><br><br><br>"));
		return v;
	}

	

}
