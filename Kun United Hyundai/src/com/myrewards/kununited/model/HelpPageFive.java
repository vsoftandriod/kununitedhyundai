package com.myrewards.kununited.model;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.myrewards.kununited.controller.R;
import com.myrewards.kununited.utils.Utility;

public class HelpPageFive extends Fragment {
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v=(LinearLayout)inflater.inflate(R.layout.page5_frag5_layout, container, false);
		TextView textview1=(TextView)v.findViewById(R.id.page1_IVID);
		textview1.setTypeface(Utility.font_reg);
		textview1.setMovementMethod(new ScrollingMovementMethod());
		textview1.setText(Html.fromHtml("<h4>  Your selected offer...  </h4>&#9733; You will  now see the offer  details, including the address, contact details of the  merchant, and all terms and conditions.<br> <br>&#9733;   If you like the offer add it to \"your favourites\" by clicking the heart. <br><br><br><br><br><br><br><br>"));
		return v;
	}
}
