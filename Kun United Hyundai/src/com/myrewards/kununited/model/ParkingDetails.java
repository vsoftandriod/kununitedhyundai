package com.myrewards.kununited.model;

public class ParkingDetails {
private String startTime;
private String endTime;
private String lattitude;
private String longittude;
private String id;
public String getStartTime() {
	return startTime;
}
public void setStartTime(String startTime) {
	this.startTime = startTime;
}
public String getEndTime() {
	return endTime;
}
public void setEndTime(String endTime) {
	this.endTime = endTime;
}
public String getLattitude() {
	return lattitude;
}
public void setLattitude(String lattitude) {
	this.lattitude = lattitude;
}
public String getLongittude() {
	return longittude;
}
public void setLongittude(String longittude) {
	this.longittude = longittude;
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}

}
